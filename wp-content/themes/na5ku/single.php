<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
    <section class="page_section_wrap">
        <div class="container">
            <div class="how_to_order_section">
                <div class="seo_text page_desc blog_single">
                    <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
                    <div class="how_it_works_title page_title">

                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="<?php the_title(); ?>">
                      <h1><?php the_title(); ?></h1>
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-left-4.svg" alt="<?php the_title(); ?>">
                  </div>
                  <?php if(get_the_post_thumbnail_url( get_the_ID(), 'large' )){ ?>
                    <img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'large' ); ?>" class="how_to_order_img" alt="<?php the_title(); ?>">
                <?php } ?>
                <p><?php the_content(); ?></p>
            </div>
        </div>
    </div>
</section>
<?php 
$categories = get_the_category(get_the_ID());
$category_ids = array();
foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
$args=array(
    'category__in' => $category_ids,
    'post__not_in' => array(get_the_ID()),
    'showposts' => '6',
    'orderby' => 'rand',
    'ignore_sticky_posts' => '1',
    'no_found_rows' => true,
    'cache_results' => false
);
$my_query = new WP_Query($args);

if ( $my_query->have_posts() ) : ?>
    <section class="main_news_section_wrap">
        <div class="container">
            <div class="main_news_section">
                <div class="main_news_head">
                    <div class="page_title dark news popular_single">
                        <span><?php echo esc_html__('Популярное в блоге','na5ku'); ?></span>
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="популярное в блоге">
                    </div>
                </div>
                <div class="main_news_slider" id="main_news_slider">
                    <?php while($my_query->have_posts()) : $my_query->the_post(); ?>
                        <div class="main_news_item_wrap">
                            <div class="main_news_item">
                                <a href="<?php the_permalink() ?>"><img src="<?php echo (get_the_post_thumbnail_url( get_the_ID(), 'large' )) ? get_the_post_thumbnail_url( get_the_ID(), 'large' ) : esc_url( get_template_directory_uri() )."/assets/img/no-image.png"; ?>" alt="<?php the_title(); ?>" class="main_news_item_img"></a>
                                <div class="main_news_item_content">
                                    <div class="main_news_item_title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
                                    <div class="main_news_item_text">
                                        <p><?php the_excerpt(); ?></p>
                                    </div>
                                    <div class="main_news_item_bottom">
                                        <div class="main_news_item_date">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/news_calendar.svg" alt="calendar">
                                            <span><?php the_date('d.m.Y'); ?></span>
                                        </div>
                                        <a href="<?php the_permalink() ?>" class="main_news_item_more_link"><?php echo esc_html__('Подробнее','na5ku'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile;
                    wp_reset_postdata();
                    ?>


                </div>
            </div>
        </div>
    </section>  

<?php endif; ?>
<?php endwhile; ?>

<?php get_footer(); ?>
