<?php get_header(); ?>

<?php if ( get_field( 'home_title_1', 'option' ) ) : ?>
	<section class="main_first_section_wrap">
		<div class="container">
			<div class="main_first_section">
				<div class="main_first_title">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-3.svg" alt="sec1">
					<span><?php the_field( 'home_title_1', 'option' ); ?></span>
				</div>
				<?php if ( get_field( 'home_descr_1', 'option' ) ) : ?>
					<div class="main_first_desc">
						<?php the_field( 'home_descr_1', 'option' ); ?>
					</div>
				<?php endif; ?>
				<?php if ( have_rows( 'home_adv_1', 'option' ) ) : ?>
					<?php while ( have_rows( 'home_adv_1', 'option' ) ) : the_row(); ?>
						<div class="main_first_item">
							<?php if ( get_sub_field( 'home_adv_icon_1' ) ) : ?>
								<img src="<?php the_sub_field( 'home_adv_icon_1' ); ?>" />
							<?php endif ?>
							<span><?php the_sub_field( 'home_adv_text_1' ); ?></span>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php if ( get_field( 'home_btn_title_1', 'option' ) ) : ?>
					<a href="<?php the_field( 'home_btn_link_1', 'option' ); ?>" class="main_first_btn blue_btn"><?php the_field( 'home_btn_title_1', 'option' ); ?></a>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<section class="calculate_price_section_wrap" id="order_form">
        <div class="container">
            <div class="calculate_price_section main">
                <div class="calculate_price_left">
                    <div class="calculate_price_title"><?php the_field( 'home_form_order_title', 'option' ); ?></div>
                    <div class="calculate_price_desc"><?php the_field( 'home_form_order_descr', 'option' ); ?></div>
                    <?php if ( get_field( 'short_code_form_order', 'option' ) ) { ?>
                    	<?php echo do_shortcode( get_field( 'short_code_form_order', 'option' ) ); ?>
                    <?php } else { ?>
                    	<?php 
                    	$form_lang = ( get_field( 'user_country', 'option' ) == 1 ) ? 'form_order_uk.php' : 'form_order.php';
                    	include_once get_template_directory() . '/inc/form/'.$form_lang; ?>
                    <?php } ?>
                </div>
            </div>
        </div>
</section>

<?php if ( get_field( 'home_title_2', 'option' ) ) : ?>
	<section class="we_write_section_wrap" id="we_write_section_wrap">
		<div class="container">
			<div class="we_write_section">
				<div class="page_title white">
					<span><?php the_field( 'home_title_2', 'option' ); ?></span>
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="play">
				</div>
				<div class="we_write_offers_list">
					<?php if ( have_rows( 'home_service_list_2', 'option' ) ) : ?>

						<?php while ( have_rows( 'home_service_list_2', 'option' ) ) : the_row(); ?>
							<div class="we_write_offer_item">
								<?php $home_service_price_url = get_sub_field( 'home_service_price_url' ); ?>
								<div class="we_write_offer_item_title">
									<a href="<?php echo esc_url( $home_service_price_url); ?>">
										<span><?php the_sub_field( 'home_service_title' ); ?></span>
									</a>
									<img src="<?php the_sub_field( 'home_service_icon' ); ?>" alt="<?php the_sub_field( 'home_service_title' ); ?>">
								</div>
								<div class="we_write_offer_item_content">

									<?php if ( have_rows( 'home_service_descr' ) ) : ?>
										<?php while ( have_rows( 'home_service_descr' ) ) : the_row(); ?>
											<div class="we_write_offer_item_content_row">
												<div class="we_write_offer_item_content_title"><?php the_sub_field( 'home_service_descr_title' ); ?></div>
												<div class="we_write_offer_item_content_text"><?php the_sub_field( 'home_service_descr_short' ); ?></div>
											</div>
										<?php endwhile; ?>

									<?php endif; ?>

								</div>
								<div class="we_write_offer_item_price"><?php echo esc_html__('Цена:','na5ku'); ?> <span><?php the_sub_field( 'home_service_price' ); ?></span></div>
								<div class="we_write_offer_item_btn_wrap">
									<button onclick="javascript:location.href='#order_form'" class="we_write_offer_item_btn"><?php echo esc_html__('Заказать','na5ku'); ?></button>
								</div>
							</div>
						<?php endwhile; ?>

					<?php endif; ?>

				</div>

				<div class="page_title white small">
					<span><?php the_field( 'home_add_title_2', 'option' ); ?></span>
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="">
				</div>
				<div class="we_write_additional_list">
					<?php if ( have_rows( 'home_add_service_list_2', 'option' ) ) : ?>
						<?php while ( have_rows( 'home_add_service_list_2', 'option' ) ) : the_row(); ?>
							<?php $home_add_service_url = get_sub_field( 'home_add_service_url' ); ?>
							<div class="we_write_additional_item">
								<a href="<?php echo esc_url( $home_add_service_url); ?>"><div class="we_write_additional_item_title"><?php the_sub_field( 'home_add_service_name' ); ?></div>
									<div class="we_write_additional_item_price"><?php echo esc_html__('Цена:','na5ku'); ?> <span><?php the_sub_field( 'home_add_service_price' ); ?></span></div></a>
								</div>

							<?php endwhile; ?>
							<?php endif; ?>

						</div>
					</div>
				</div>
			</section>
		<?php endif; ?>

		<?php include_once get_template_directory() . '/inc/section_example_work.php'; ?>

		<?php if ( get_field( 'home_title_3', 'option' ) ) : ?>
			<section class="how_it_works_section_wrap" id="how_it_works_section_wrap">
				<div class="container">
					<div class="how_it_works_section">
						<div class="how_it_works_title">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="">
							<span><?php the_field( 'home_title_3', 'option' ); ?></span>
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-left-4.svg" alt="">
						</div>
						<div class="how_it_works_tabs">
							<div class="how_it_works_steps">
								<?php if ( have_rows( 'home_slide_3', 'option' ) ) : ?>
									<?php $total_rows = count(get_field('home_slide_3', 'option')); while ( have_rows( 'home_slide_3', 'option' ) ) : the_row(); ?>
									<?php $active = (get_row_index() == 1) ? 'active' : ''; ?>
									<div class="how_it_works_step_number <?php echo $active; ?>"><?php echo get_row_index(); ?></div>
									<?php if($total_rows != get_row_index()){ ?>
										<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/how_it_works_dots_arr.svg" alt="">
									<?php } ?>

								<?php endwhile; ?>

							<?php endif; ?>

						</div>

						<div class="how_it_works_links">
							<?php if ( have_rows( 'home_slide_3', 'option' ) ) : ?>
								<?php while ( have_rows( 'home_slide_3', 'option' ) ) : the_row(); ?>
									<?php $active = (get_row_index() == 1) ? 'active' : ''; ?>
									<div class="how_it_works_link_item <?php echo $active; ?>"><?php the_sub_field( 'home_slide_title_3' ); ?> </div>

								<?php endwhile; ?>

							<?php endif; ?>
						</div>
						<div class="how_it_works_tab_content">
							<?php if ( have_rows( 'home_slide_3', 'option' ) ) : ?>
								<?php while ( have_rows( 'home_slide_3', 'option' ) ) : the_row(); ?>
									<?php $active = (get_row_index() == 1) ? 'active' : ''; ?>
									<img class="how_it_works_tab_item <?php echo $active; ?>" src="<?php the_sub_field( 'home_slide_img_3' ); ?>" alt="<?php the_sub_field( 'home_slide_title_3' ); ?>">
								<?php endwhile; ?>

							<?php endif; ?>
						</div>

					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>

	<?php if ( get_field( 'home_faq_title_6', 'option' ) ) : ?>
		<section class="faq_section_wrap">
			<div class="container">
				<div class="faq_section">
					<div class="page_title white">
						<span><?php the_field( 'home_faq_title_6', 'option' ); ?></span>
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="<?php the_field( 'home_faq_title_6', 'option' ); ?>">
					</div>
					<div class="faq_accordion">
						<?php if ( have_rows( 'home_faq_list', 'option' ) ) : ?>
							<?php while ( have_rows( 'home_faq_list', 'option' ) ) : the_row(); ?>
								<div class="faq_accordion_item">
									<div class="faq_accordion_item_head">
										<div class="faq_accordion_item_head_num_wrap">
											<div class="faq_accordion_item_head_num"><?php echo get_row_index() ; ?></div>
										</div>
										<div class="faq_accordion_item_head_title">
											<span><?php the_sub_field( 'home_faq_quest' ); ?></span>
											<div class="faq_accordion_item_head_title_symbol"></div>
										</div>
									</div>
									<div class="faq_accordion_item_content"><?php the_sub_field( 'home_faq_answer' ); ?></div>
								</div>

							<?php endwhile; ?>

						<?php endif; ?>


					</div>
					<a href="<?php the_field( 'home_faq_link_btn_6', 'option' ); ?>" class="faq_all_questions_btn blue_btn"><?php the_field( 'home_faq_btn_6', 'option' ); ?></a>
				</div>
			</div>
		</section>
	<?php endif; ?>

	<?php if ( get_field( 'front-news-cat', 'option' ) ) : ?>
		<section class="main_news_section_wrap">
			<div class="container">
				<div class="main_news_section">
					<div class="main_news_head">
						<div class="page_title dark news">
							<span><?php the_field( 'home_blog_title_7', 'option' ); ?></span>
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="<?php the_field( 'home_blog_title_7', 'option' ); ?>">
						</div>
						<a href="<?php the_field( 'home_blog_btn_url_7', 'option' ); ?>" class="main_news_head_all_btn blue_btn"><?php the_field( 'home_blog_btn_title_7', 'option' ); ?></a>
					</div>
					<div class="main_news_slider" id="main_news_slider">
						<?php $catquery = new WP_Query( 'cat='.get_field('front-news-cat', 'option' ).'&posts_per_page='.get_field('front-news-count', 'option' ) ); ?>

						<?php while($catquery->have_posts()) : $catquery->the_post(); ?>
							<div class="main_news_item_wrap">
								<div class="main_news_item">
									<a href="<?php the_permalink() ?>"><img src="<?php echo (get_the_post_thumbnail_url( get_the_ID(), 'large' )) ? get_the_post_thumbnail_url( get_the_ID(), 'large' ) : esc_url( get_template_directory_uri() )."/assets/img/no-image.png"; ?>" alt="<?php the_title(); ?>" class="main_news_item_img"></a>
									<div class="main_news_item_content">
										<div class="main_news_item_title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
										<div class="main_news_item_text">
											<p><?php the_excerpt(); ?></p>
										</div>
										<div class="main_news_item_bottom">
											<div class="main_news_item_date">
												<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/news_calendar.svg" alt="calendar">
												<span><?php the_date('d.m.Y'); ?></span>
											</div>
											<a href="<?php the_permalink() ?>" class="main_news_item_more_link"><?php echo esc_html__('Подробнее','na5ku'); ?></a>
										</div>
									</div>
								</div>
							</div>
						<?php endwhile;
						wp_reset_postdata();
						?>


					</div>
				</div>
			</div>
		</section>	

	<?php endif; ?>

	<?php if ( get_field( 'home_seo_title_8', 'option' ) ) : ?>
		<section class="how_to_order_section_wrap" id="how_to_order_section_wrap">
			<div class="container">
				<div class="how_to_order_section">
					<div class="seo_text">
						<div class="how_to_order_title">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="<?php the_field( 'home_seo_title_8', 'option' ); ?>">
							<span><?php the_field( 'home_seo_title_8', 'option' ); ?></span>
						</div>
						<?php the_field( 'home_seo_text_8', 'option' ); ?>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<?php include_once get_template_directory() . '/inc/front_style.php'; ?>
	<?php get_footer(); ?>
