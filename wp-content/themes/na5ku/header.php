<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, shrink-to-fit=no">
	<?php wp_head(); ?>
	<?php if ( get_field( 'footer_logo', 'option' ) ) : ?>
		<?php the_field( 'header_scripts', 'option' ); ?>
	<?php endif ?>
</head>
<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div class="page_wrap">
		<header class="header_wrap">
			<div class="container">
				<div class="header">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php the_field( 'header_logo', 'option' ); ?>" alt="<?php bloginfo( 'name' ); ?>" class="head_logo"></a>
					<div class="head_info">
						<?php the_field( 'header_right_logo_text', 'option' ); ?>
					</div>
					<div class="head_lang">
						<?php icl_post_languages(); ?>
						
					</div>
					<div class="head_social">
						<?php if ( get_field( 'header_link_insta', 'option' ) ) : ?>
						<a href="<?php the_field( 'header_link_insta', 'option' ); ?>" class="head_social_item">
							<svg width="18" height="18" viewBox="0 0 18 18"  xmlns="http://www.w3.org/2000/svg">
								<path d="M13.1275 0H4.87243C2.18573 0 0 2.18573 0 4.87243V13.1277C0 15.8142 2.18573 18 4.87243 18H13.1277C15.8142 18 18 15.8142 18 13.1277V4.87243C18 2.18573 15.8142 0 13.1275 0V0ZM8.99998 13.9217C6.28609 13.9217 4.07825 11.7139 4.07825 8.99998C4.07825 6.28609 6.28609 4.07825 8.99998 4.07825C11.7139 4.07825 13.9217 6.28609 13.9217 8.99998C13.9217 11.7139 11.7139 13.9217 8.99998 13.9217ZM14.0394 5.23896C13.2374 5.23896 12.5851 4.58665 12.5851 3.78465C12.5851 2.98265 13.2374 2.3302 14.0394 2.3302C14.8414 2.3302 15.4939 2.98265 15.4939 3.78465C15.4939 4.58665 14.8414 5.23896 14.0394 5.23896Z"/>
								<path d="M9.00028 5.13349C6.86839 5.13349 5.13379 6.86795 5.13379 8.99998C5.13379 11.1319 6.86839 12.8665 9.00028 12.8665C11.1323 12.8665 12.8668 11.1319 12.8668 8.99998C12.8668 6.86795 11.1323 5.13349 9.00028 5.13349Z" />
								<path d="M14.0397 3.38557C13.8197 3.38557 13.6406 3.56464 13.6406 3.78464C13.6406 4.00465 13.8197 4.18372 14.0397 4.18372C14.2598 4.18372 14.4389 4.00478 14.4389 3.78464C14.4389 3.56451 14.2598 3.38557 14.0397 3.38557Z" />
							</svg>
						</a>
						<?php endif ?>
						<?php if ( get_field( 'header_link_vk', 'option' ) ) : ?>
						<a href="<?php the_field( 'header_link_vk', 'option' ); ?>" class="head_social_item">
							<svg width="18" height="12" viewBox="0 0 18 12" xmlns="http://www.w3.org/2000/svg">
								<path d="M14.9363 6.771C14.6453 6.4035 14.7285 6.24 14.9363 5.9115C14.94 5.90775 17.3423 2.58825 17.5898 1.4625L17.5913 1.46175C17.7143 1.0515 17.5913 0.75 16.9965 0.75H15.0285C14.5275 0.75 14.2965 1.00875 14.1728 1.29825C14.1728 1.29825 13.1708 3.69675 11.7533 5.2515C11.2958 5.70075 11.0843 5.84475 10.8345 5.84475C10.7115 5.84475 10.5203 5.70075 10.5203 5.2905V1.46175C10.5203 0.96975 10.38 0.75 9.96525 0.75H6.87075C6.5565 0.75 6.36975 0.9795 6.36975 1.19325C6.36975 1.65975 7.0785 1.767 7.152 3.0795V5.92725C7.152 6.55125 7.03875 6.666 6.7875 6.666C6.1185 6.666 4.49475 4.25775 3.5325 1.5015C3.33825 0.96675 3.1485 0.75075 2.64375 0.75075H0.675C0.11325 0.75075 0 1.0095 0 1.299C0 1.8105 0.669 4.35375 3.111 7.71375C4.7385 10.0073 7.0305 11.25 9.1155 11.25C10.3688 11.25 10.5218 10.974 10.5218 10.4993C10.5218 8.30776 10.4085 8.10075 11.0363 8.10075C11.3273 8.10075 11.8283 8.24476 12.9983 9.35101C14.3355 10.6628 14.5553 11.25 15.3038 11.25H17.2718C17.8328 11.25 18.117 10.974 17.9535 10.4295C17.5793 9.28426 15.0503 6.9285 14.9363 6.771Z"/>
							</svg>
						</a>
						<?php endif ?>
					</div>
					<?php if ( get_field( 'header_phone', 'option' ) ) : ?>
					<div class="head_contacts">
						<div class="head_contacts_tel">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/foot_tel.svg" alt="">
							<span><?php the_field( 'header_phone', 'option' ); ?></span>
						</div>
						<div class="head_contacts_text"><?php the_field( 'header_phone_text', 'option' ); ?></div>
					</div>
					<?php endif ?>
					<?php if ( get_field( 'header_order_btn_title', 'option' ) == 1) : ?>
					<a href="/#order_form" class="head_order_btn blue_btn"><?php echo esc_html__('Оформить заказ','na5ku'); ?></a>
					<?php endif ?>
					<?php if(get_field( 'header_auth_on', 'option' ) == 1) : ?>
					<!--<div class="head_sign_up">
						<span class="head_login"><a href="#reg"><?php echo esc_html__('Войти','na5ku'); ?></a></span>
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/head_user.svg" alt="" class="head_login_user">
						<span class="head_reg"><a href="#reg"><?php echo esc_html__('Регистрация','na5ku'); ?></a></span>
					</div>-->
					<?php echo do_shortcode( get_field( 'short_code_form_auth', 'option' ) ); ?>
					<?php endif ?>
				</div>
			</div>
		</header>
		<div class="top_nav_wrap">
			<div class="container">
					<?php wp_nav_menu( [ 
						'container'      => false,
						'fallback_cb'    => false,
						'echo'          => true,
						'theme_location'  => 'primary',
						'walker'         => new Header_Walker_Nav_Menu(),
						'bem_block'      => 'nav',
						'items_wrap'     => '
						<nav class="top_nav">
						<ul class="top_nav_list">%3$s</ul>
						</nav>
						'
					] ); ?>
			</div>
		</div>
		<div class="mobile_header">
			<div class="mobile_header_top_wrap">
				<div class="container">
					<div class="mobile_header_top">
						<a href="/"><img src="<?php the_field( 'header_logo', 'option' ); ?>" alt="" class="mobile_header_logo"></a>
						<?php if ( get_field( 'header_order_btn_title', 'option' ) == 1) : ?>
						<a href="#order_form" class="mobile_header_order_btn"><?php echo esc_html__('Оформить заказ','na5ku'); ?></a>
						<?php endif ?>
						<div class="mobile_header_ham">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>
				</div>
			</div>
			<div class="mobile_header_bottom_slide">
				<div class="mobile_header_bottom">
					<?php wp_nav_menu( [ 
						'container'      => false,
						'fallback_cb'    => false,
						'echo'          => true,
						'theme_location'  => 'primary',
						'walker'         => new Mobile_Walker_Nav_Menu(),
						'bem_block'      => 'nav',
						'items_wrap'     => '<ul class="mobile_header_nav">%3$s</ul>'
					] ); ?>
					<div class="mobile_header_bottom_row">
						<?php if ( get_field( 'header_phone', 'option' ) ) : ?>
						<div class="head_contacts">
							<div class="head_contacts_tel">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/foot_tel.svg" alt="">
								<span><?php the_field( 'header_phone', 'option' ); ?></span>
							</div>
							<div class="head_contacts_text"><?php the_field( 'header_phone_text', 'option' ); ?></div>
						</div>
						<?php endif ?>
						<div class="head_social">
							<?php if ( get_field( 'header_link_insta', 'option' ) ) : ?>
							<a href="<?php the_field( 'header_link_insta', 'option' ); ?>" class="head_social_item">
								<svg width="18" height="18" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
									<path d="M13.1275 0H4.87243C2.18573 0 0 2.18573 0 4.87243V13.1277C0 15.8142 2.18573 18 4.87243 18H13.1277C15.8142 18 18 15.8142 18 13.1277V4.87243C18 2.18573 15.8142 0 13.1275 0V0ZM8.99998 13.9217C6.28609 13.9217 4.07825 11.7139 4.07825 8.99998C4.07825 6.28609 6.28609 4.07825 8.99998 4.07825C11.7139 4.07825 13.9217 6.28609 13.9217 8.99998C13.9217 11.7139 11.7139 13.9217 8.99998 13.9217ZM14.0394 5.23896C13.2374 5.23896 12.5851 4.58665 12.5851 3.78465C12.5851 2.98265 13.2374 2.3302 14.0394 2.3302C14.8414 2.3302 15.4939 2.98265 15.4939 3.78465C15.4939 4.58665 14.8414 5.23896 14.0394 5.23896Z"></path>
									<path d="M9.00028 5.13349C6.86839 5.13349 5.13379 6.86795 5.13379 8.99998C5.13379 11.1319 6.86839 12.8665 9.00028 12.8665C11.1323 12.8665 12.8668 11.1319 12.8668 8.99998C12.8668 6.86795 11.1323 5.13349 9.00028 5.13349Z"></path>
									<path d="M14.0397 3.38557C13.8197 3.38557 13.6406 3.56464 13.6406 3.78464C13.6406 4.00465 13.8197 4.18372 14.0397 4.18372C14.2598 4.18372 14.4389 4.00478 14.4389 3.78464C14.4389 3.56451 14.2598 3.38557 14.0397 3.38557Z"></path>
								</svg>
							</a>
							<?php endif ?>
							<?php if ( get_field( 'header_link_vk', 'option' ) ) : ?>
							<a href="<?php the_field( 'header_link_vk', 'option' ); ?>" class="head_social_item">
								<svg width="18" height="12" viewBox="0 0 18 12" xmlns="http://www.w3.org/2000/svg">
									<path d="M14.9363 6.771C14.6453 6.4035 14.7285 6.24 14.9363 5.9115C14.94 5.90775 17.3423 2.58825 17.5898 1.4625L17.5913 1.46175C17.7143 1.0515 17.5913 0.75 16.9965 0.75H15.0285C14.5275 0.75 14.2965 1.00875 14.1728 1.29825C14.1728 1.29825 13.1708 3.69675 11.7533 5.2515C11.2958 5.70075 11.0843 5.84475 10.8345 5.84475C10.7115 5.84475 10.5203 5.70075 10.5203 5.2905V1.46175C10.5203 0.96975 10.38 0.75 9.96525 0.75H6.87075C6.5565 0.75 6.36975 0.9795 6.36975 1.19325C6.36975 1.65975 7.0785 1.767 7.152 3.0795V5.92725C7.152 6.55125 7.03875 6.666 6.7875 6.666C6.1185 6.666 4.49475 4.25775 3.5325 1.5015C3.33825 0.96675 3.1485 0.75075 2.64375 0.75075H0.675C0.11325 0.75075 0 1.0095 0 1.299C0 1.8105 0.669 4.35375 3.111 7.71375C4.7385 10.0073 7.0305 11.25 9.1155 11.25C10.3688 11.25 10.5218 10.974 10.5218 10.4993C10.5218 8.30776 10.4085 8.10075 11.0363 8.10075C11.3273 8.10075 11.8283 8.24476 12.9983 9.35101C14.3355 10.6628 14.5553 11.25 15.3038 11.25H17.2718C17.8328 11.25 18.117 10.974 17.9535 10.4295C17.5793 9.28426 15.0503 6.9285 14.9363 6.771Z"></path>
								</svg>
							</a>
							<?php endif ?>
						</div>
						<?php if(get_field( 'header_auth_on', 'option' ) == 1) : ?>
						<?php echo do_shortcode( get_field( 'short_code_form_auth', 'option' ) ); ?>
						<!--<button class="mobile_header_signup_btn">
							<span><a href="#auth"><?php echo esc_html__('Войти','na5ku'); ?></a> <a href="#reg"><?php echo esc_html__('Регистрация','na5ku'); ?></a></span>
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/head_user.svg" alt="">
						</button>-->
						<?php endif ?>
						<div class="head_lang">
							<?php icl_post_languages(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
