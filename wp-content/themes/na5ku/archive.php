<?php get_header(); ?>

<section class="main_news_section_wrap">
	<div class="container">
		<div class="main_news_section">
			<div class="main_news_head">
				<div class="page_title dark news">
					<span><?php single_cat_title(); ?></span>
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="<?php the_field( 'home_blog_title_7', 'option' ); ?>">
				</div>
			</div>
			<div class="main_news_slider category_slider">
				<?php
				while ( have_posts() ) :
					the_post(); ?>
					<div class="main_news_item_wrap">
						<div class="main_news_item">
							<a href="<?php the_permalink() ?>"><img src="<?php echo (get_the_post_thumbnail_url( get_the_ID(), 'large' )) ? get_the_post_thumbnail_url( get_the_ID(), 'large' ) : esc_url( get_template_directory_uri() )."/assets/img/no-image.png"; ?>" alt="<?php the_title(); ?>" class="main_news_item_img"></a>
							<div class="main_news_item_content">
								<div class="main_news_item_title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
								<div class="main_news_item_text">
									<p><?php the_excerpt(); ?></p>
								</div>
								<div class="main_news_item_bottom">
									<div class="main_news_item_date">
										<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/news_calendar.svg" alt="calendar">
										<span><?php the_date('d.m.Y'); ?></span>
									</div>
									<a href="<?php the_permalink() ?>" class="main_news_item_more_link"><?php echo esc_html__('Подробнее','na5ku'); ?></a>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
				<?php the_posts_pagination(); ?>
			</div>
		</div>
	</div>
</section>	
<?php get_footer(); ?>
