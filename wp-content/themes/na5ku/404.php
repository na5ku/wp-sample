<?php get_header(); ?>
	<section class="page_section_wrap">
		<div class="container">
			<div class="page_section">
				<div class="page_title">
					<div class="how_it_works_title">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="<?php the_title(); ?>">
						<h1><?php _e( 'Oops! That page can&rsquo;t be found.', 'na5ku' ); ?></h1>
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-left-4.svg" alt="<?php the_title(); ?>">
					</div>
				</div>
				<div class="page_desc"><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'na5ku' ); ?></div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>
