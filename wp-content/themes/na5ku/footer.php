    <footer class="footer_wrap" id="footer_wrap">
        <div class="container">
            <div class="footer">
                <div class="footer_top">
                    <div class="footer_col info">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php the_field( 'footer_logo', 'option' ); ?>" alt="<?php bloginfo( 'name' ); ?>" class="foot_logo"></a>
                        <div class="foot_contacts">
                            <div class="foot_tel">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/foot_tel.svg" alt="">
                                <span><?php the_field( 'footer_phone', 'option' ); ?></span>
                            </div>
                            <div class="foot_tel_text"><?php the_field( 'footer_phone_text', 'option' ); ?></div>
                        </div>
                        <?php if ( have_rows( 'foot_payments', 'option' ) ) : ?>
                        <div class="foot_payments">
                            <?php while ( have_rows( 'foot_payments', 'option' ) ) : the_row(); ?>
                            <div class="foot_payments_item">
                                <img src="<?php the_sub_field( 'foot_payments_icon' ); ?>" alt="icon">
                            </div>
                            <?php endwhile; ?>
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php wp_nav_menu( [
                        'theme_location'  => 'footer',
                        'menu'            => '', 
                        'container'       => 'div', 
                        'container_class' => 'footer_col list', 
                        'container_id'    => '',
                        'menu_class'      => 'foot_list', 
                        'menu_id'         => '',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'before'          => '',
                        'after'           => '',
                        'link_before'     => '',
                        'link_after'      => '',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth'           => 0,
                        'walker'          => '',
                    ] ); ?>
                    <?php if ( is_active_sidebar( 'sidebar-footer-2' ) ) : ?>
                        <div class="footer_col list">
                            <ul class="foot_list">
                                <?php dynamic_sidebar( 'sidebar-footer-2' ); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'sidebar-footer-2' ) ) : ?>
                        <div class="footer_col list list_two">
                            <ul class="foot_list">
                                <?php dynamic_sidebar( 'sidebar-footer-3' ); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="footer_bottom">
                    <div class="foot_copy"><?php echo esc_html__('© Copyright na5ku.com.ua | Киев 2013-2021','na5ku'); ?></div>
                    <?php if ( is_active_sidebar( 'sidebar-footer' ) ) : ?>
                        <?php dynamic_sidebar( 'sidebar-footer' ); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </footer>
</div>

<?php wp_footer(); ?>
<?php include_once get_template_directory() . '/inc/main_style.php'; ?>
</body>
</html>
