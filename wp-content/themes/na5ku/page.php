<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<section class="page_section_wrap">
		<div class="container">
			<div class="page_section">
				<div class="page_title">
					<div class="how_it_works_title">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="<?php the_title(); ?>">
						<h1><?php the_title(); ?></h1>
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-left-4.svg" alt="<?php the_title(); ?>">
					</div>
				</div>
				<div class="page_desc"><?php the_content(); ?></div>
			</div>
		</div>
	</section>
<?php endwhile; ?>

<?php get_footer(); ?>
