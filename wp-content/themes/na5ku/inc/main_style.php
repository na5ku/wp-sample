<style>
<?php if ( get_field( 'color_menu_wrap', 'option' ) ) : ?>
	.top_nav_wrap {
		background-color: <?php the_field( 'color_menu_wrap', 'option' ); ?>;
	}
	.mobile_header_nav {
    background-color: <?php the_field( 'color_menu_wrap', 'option' ); ?>;
}
<?php endif ?>
<?php if ( get_field( 'color_btn_blue', 'option' ) ) : ?>
	.blue_btn {
		background-color: <?php the_field( 'color_btn_blue', 'option' ); ?>;
		border: 1px solid <?php the_field( 'color_btn_blue', 'option' ); ?>;
	}
<?php endif ?>
<?php if ( get_field( 'color_btn_blue_text', 'option' ) ) : ?>
	.blue_btn {
		color: <?php the_field( 'color_btn_blue_text', 'option' ); ?>;
	}
<?php endif ?>
<?php if ( get_field( 'color_btn_header_soc', 'option' ) ) : ?>
.head_social_item svg {
    fill: <?php the_field( 'color_btn_header_soc', 'option' ); ?>;
}
<?php endif ?>
<?php if ( get_field( 'image-back-footer-pc', 'option' ) ) : ?>
.footer_wrap {
    background-image: url(<?php the_field( 'image-back-footer-pc', 'option' ); ?>);
}
<?php endif ?>
<?php if ( get_field( 'color_dif_section', 'option' ) ) : ?>
.faq_accordion_item_head_num_wrap {
    border: 1px solid <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.faq_accordion_item.active .faq_accordion_item_head_num_wrap {
    background-color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.submenu_one_list li:hover{
    border-left-color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.mobile_header_order_btn {
    border: 1px solid <?php the_field( 'color_dif_section', 'option' ); ?>;
    color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.faq_accordion_item_head_title_symbol::after {
    background-color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.faq_accordion_item_head_title_symbol::before {
    background-color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.we_write_offer_item_btn {
    background-color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.examples_list_slider_dots li.slick-active {
    border-color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.we_write_additional_item_price span {
    color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.how_it_works_step_number.active {
    background-color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.how_it_works_step_number {
    border: 1px solid <?php the_field( 'color_dif_section', 'option' ); ?>;
    color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.faq_accordion_item_head_num {
    background-color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.main_news_item_more_link {
    color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.main_news_item_more_link::after, .main_news_item_more_link::before {
    background-color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
.examples_list_slider_dots li.slick-active button {
    background-color: <?php the_field( 'color_dif_section', 'option' ); ?>;
}
<?php endif ?>
<?php if ( get_field( 'color_header_menu_hover', 'option' ) ) : ?>
.top_nav_list > li:hover{
    background-color: <?php the_field( 'color_header_menu_hover', 'option' ); ?>;
}
<?php endif ?>

<?php if ( get_field( 'color_mobile_ham', 'option' ) ) : ?>
.mobile_header_ham {
    background-color: <?php the_field( 'color_mobile_ham', 'option' ); ?>;
}
<?php endif ?>

<?php if ( get_field( 'color_mobile_nav_active', 'option' ) ) : ?>
.mobile_header_nav li.active > a {
    background-color:<?php the_field( 'color_mobile_nav_active', 'option' ); ?>;
}
<?php endif ?>

<?php if ( get_field( 'color_mobile_nav_sub', 'option' ) ) : ?>
.mobile_header_sub_list_two {
    background-color: <?php the_field( 'color_mobile_nav_sub', 'option' ); ?>;
}
<?php endif ?>
<?php if ( get_field( 'color_mobile_nav_sub_active', 'option' ) ) : ?>
.mobile_header_sub_list li.active > a {
    background-color: <?php the_field( 'color_mobile_nav_sub_active', 'option' ); ?>;
}
<?php endif ?>
@media screen and (max-width: 767px){
<?php if ( get_field( 'image-back-footer-mob', 'option' ) ) : ?>
.footer_wrap {
    background-image: url(<?php the_field( 'image-back-footer-mob', 'option' ); ?>);
}
<?php endif ?>
} 
</style>