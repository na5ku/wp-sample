<?php if ( have_rows( 'serv_faq_list' ) ) : ?>
		<section class="faq_section_wrap">
			<div class="container">
				<div class="faq_section">
					<div class="page_title white">
						<span><?php the_field( 'serv_faq_title'); ?></span>
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="<?php the_field( 'serv_faq_title'); ?>">
					</div>
					<div class="faq_accordion">
						<?php if ( have_rows( 'serv_faq_list') ) : ?>
							<?php while ( have_rows( 'serv_faq_list') ) : the_row(); ?>
								<div class="faq_accordion_item">
									<div class="faq_accordion_item_head">
										<div class="faq_accordion_item_head_num_wrap">
											<div class="faq_accordion_item_head_num"><?php echo get_row_index() ; ?></div>
										</div>
										<div class="faq_accordion_item_head_title">
											<span><?php the_sub_field( 'serv_faq_quest' ); ?></span>
											<div class="faq_accordion_item_head_title_symbol"></div>
										</div>
									</div>
									<div class="faq_accordion_item_content"><?php the_sub_field( 'serv_faq_answ' ); ?></div>
								</div>

							<?php endwhile; ?>

						<?php endif; ?>


					</div>
					<?php if ( get_field( 'serv_faq_btn_title') ) : ?>
						<a href="<?php the_field( 'serv_faq_btn_href'); ?>" class="faq_all_questions_btn blue_btn"><?php the_field( 'serv_faq_btn_title'); ?></a>
					<?php endif; ?>
				</div>
			</div>
		</section>
	<?php endif; ?>