<?php
$countryId = intval(get_field('user_country', 'option'));

if (!$countryId) {
    $countryId = 1;
}
?>
<script>
    var NA5KU_VALUTA_THEME = <?=$countryId?>
</script>
<section class="examples_tabs_section_wrap" id="examples_tabs_section_wrap">
    <div class="container">
        <div class="examples_tabs_section">
            <div class="examples_tabs_nav">
                <a href="#tab_1" class="examples_tabs_nav_link active">
                    <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10 0C4.48622 0 0 4.48622 0 10C0 15.5138 4.48622 20 10 20C15.5138 20 20 15.5138 20 10C20 4.48622 15.5138 0 10 0ZM15.589 7.36842L9.198 13.7093C8.82206 14.0852 8.22055 14.1103 7.81955 13.7343L4.43609 10.6516C4.03509 10.2757 4.01003 9.64912 4.3609 9.24812C4.73684 8.84712 5.36341 8.82206 5.76441 9.198L8.44612 11.6541L14.1604 5.93985C14.5614 5.53885 15.188 5.53885 15.589 5.93985C15.99 6.34085 15.99 6.96742 15.589 7.36842Z"/>
                    </svg>
                    <span><?php echo esc_html__('Примеры выполненных работ', 'na5ku'); ?></span>
                </a>
                <a href="#tab_2" class="examples_tabs_nav_link">
                    <svg width="18" height="20" viewBox="0 0 18 20" xmlns="http://www.w3.org/2000/svg">
                        <path d="M14.2703 5.6757C12.7838 5.6757 11.5676 4.45949 11.5676 2.97301V0H0.756836V17.2973C0.756836 18.7838 1.97305 20 3.45953 20H14.5406C16.0271 20 17.2433 18.7838 17.2433 17.2973V5.6757H14.2703ZM9.40547 14.4595H4.54062V13.1081H9.40547V14.4595ZM13.4595 11.2162H4.54059V9.86488H13.4595V11.2162Z"/>
                    </svg>
                    <span><?php echo esc_html__('Последние выполненные работы', 'na5ku'); ?></span>
                </a>
                <a href="#tab_3" class="examples_tabs_nav_link">
                    <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.0014 0C4.56561 0 0.00190886 4.06577 0.00190886 9.2636C0.00190886 11.6089 0.933616 13.8166 2.6361 15.5298C1.54384 17.3714 0.155657 19.0176 0.140813 19.0351C-0.199104 19.4361 0.123157 20.0531 0.651276 19.9963C0.784282 19.9818 3.85026 19.6314 6.56875 17.9665C13.0283 20.1553 20.0007 15.7436 20.0007 9.2636C20.0008 4.06312 15.4341 0 10.0014 0V0ZM5.60708 6.37301C5.93067 6.37301 6.19297 6.63535 6.19297 6.95894C6.19297 7.28253 5.93067 7.54487 5.60708 7.54487C5.28349 7.54487 5.02118 7.28253 5.02118 6.95894C5.02115 6.63535 5.28349 6.37301 5.60708 6.37301ZM11.9543 12.1932H5.60708C5.28349 12.1932 5.02118 11.9309 5.02118 11.6073C5.02118 11.2837 5.28353 11.0214 5.60708 11.0214H11.9544C12.278 11.0214 12.5403 11.2837 12.5403 11.6073C12.5403 11.9309 12.2779 12.1932 11.9543 12.1932ZM14.3956 12.1933C14.072 12.1933 13.8097 11.9309 13.8097 11.6073C13.8097 11.2837 14.072 11.0214 14.3956 11.0214C14.7192 11.0214 14.9815 11.2837 14.9815 11.6073C14.9815 11.9309 14.7192 12.1933 14.3956 12.1933ZM14.3956 9.84953H5.60708C5.28349 9.84953 5.02118 9.58719 5.02118 9.2636C5.02118 8.94001 5.28353 8.67767 5.60708 8.67767H14.3956C14.7192 8.67767 14.9815 8.94001 14.9815 9.2636C14.9815 9.58719 14.7192 9.84953 14.3956 9.84953ZM14.3956 7.50581H7.93115C7.60756 7.50581 7.34526 7.24347 7.34526 6.91988C7.34526 6.59629 7.60756 6.33395 7.93115 6.33395H14.3956C14.7192 6.33395 14.9815 6.59629 14.9815 6.91988C14.9815 7.24347 14.7192 7.50581 14.3956 7.50581Z"/>
                    </svg>
                    <span><?php echo esc_html__('Отзывы', 'na5ku'); ?></span>
                </a>
            </div>
            <div class="examples_tabs_list">
                <div class="examples_tabs_content active" id="tab_1">
                    <div class="examples_list_slider" id="examples_list_slider">
                        <?php if (have_rows('opt_serv_example_work', 'option')) : ?>
                            <?php while (have_rows('opt_serv_example_work', 'option')) : the_row(); ?>

                                <div class="examples_list_slider_item_wrap">
                                    <div class="examples_list_slider_item">
                                        <div class="examples_list_slider_item_title"><?php the_sub_field('opt_serv_example_work_title'); ?></div>
                                        <div class="examples_list_slider_item_content">
                                            <?php if (have_rows('opt_serv_example_work_data')) : ?>
                                                <?php while (have_rows('opt_serv_example_work_data')) : the_row(); ?>
                                                    <div class="we_write_offer_item_content_row">
                                                        <div class="we_write_offer_item_content_title"><?php the_sub_field('opt_serv_example_work_add_title'); ?></div>
                                                        <div class="we_write_offer_item_content_text"><?php the_sub_field('opt_serv_example_work_add_descr'); ?></div>
                                                    </div>
                                                <?php endwhile; ?>
                                            <?php endif; ?>

                                            <a href="<?php echo (get_sub_field('opt_serv_example_work_file')) ? get_sub_field('opt_serv_example_work_file') : get_sub_field('opt_serv_example_work_file_href'); ?>"
                                               class="examples_list_slider_item_download_btn" download="download">
   												<span class="examples_list_slider_item_btn_left">
   													<svg width="28" height="30" viewBox="0 0 28 30"
                                                         xmlns="http://www.w3.org/2000/svg">
   														<path d="M7.73501 15.3718C7.73501 14.5639 7.17406 14.0818 6.18414 14.0818C5.78033 14.0818 5.50674 14.1207 5.36328 14.1598V16.7538C5.53289 16.7923 5.74138 16.8055 6.02733 16.8055C7.08348 16.8055 7.73501 16.2713 7.73501 15.3718Z"/>
   														<path d="M13.8626 14.1077C13.419 14.1077 13.1329 14.1467 12.9629 14.1858V19.9332C13.1326 19.9729 13.4064 19.9729 13.6541 19.9729C15.4524 19.9855 16.6254 18.9955 16.6254 16.897C16.6386 15.0717 15.5696 14.1077 13.8626 14.1077Z"/>
   														<path d="M25.6026 10.7309H24.8822V7.25505C24.8822 7.23337 24.8788 7.21153 24.8759 7.18958C24.8747 7.05157 24.8308 6.916 24.7363 6.80859L18.9522 0.201198C18.9505 0.199509 18.9488 0.19891 18.9476 0.197058C18.9131 0.158551 18.8729 0.126525 18.8304 0.0988016C18.8178 0.0903593 18.8051 0.0832787 18.792 0.0759802C18.7551 0.0558822 18.7161 0.0392156 18.6759 0.0271786C18.665 0.0241285 18.6552 0.0196623 18.6443 0.01683C18.6006 0.00648147 18.5553 0 18.5093 0H4.29402C3.6449 0 3.11756 0.52794 3.11756 1.17652V10.7306H2.39735C1.46876 10.7306 0.71582 11.4832 0.71582 12.4121V21.1564C0.71582 22.0847 1.46876 22.8378 2.39735 22.8378H3.11761V28.8235C3.11761 29.472 3.64495 30 4.29408 30H23.7058C24.3543 30 24.8823 29.4721 24.8823 28.8235V22.8378H25.6026C26.5309 22.8378 27.284 22.0847 27.284 21.1564V12.4124C27.284 11.4835 26.5309 10.7309 25.6026 10.7309ZM4.29402 1.17674H17.9211V7.19596C17.9211 7.52096 18.1847 7.78419 18.5093 7.78419H23.7057V10.7312H4.29402V1.17674ZM18.7362 16.8313C18.7362 18.513 18.1238 19.6725 17.2754 20.3897C16.3511 21.1586 14.9432 21.5234 13.2234 21.5234C12.1939 21.5234 11.4643 21.4582 10.9684 21.3933V12.765C11.6982 12.6479 12.6502 12.5827 13.6538 12.5827C15.3211 12.5827 16.4031 12.8827 17.2507 13.5209C18.1626 14.1988 18.7362 15.2803 18.7362 16.8313ZM3.39533 21.4324V12.765C4.00759 12.6613 4.8682 12.5827 6.08029 12.5827C7.30529 12.5827 8.17871 12.8174 8.76498 13.2868C9.32554 13.7297 9.70261 14.4597 9.70261 15.32C9.70261 16.1805 9.41667 16.9103 8.89494 17.4052C8.21727 18.044 7.21384 18.3309 6.04053 18.3309C5.78029 18.3309 5.54489 18.3177 5.36325 18.2915V21.4326H3.39533V21.4324ZM23.7058 28.505H4.29402V22.8378H23.7057V28.505H23.7058ZM25.4104 14.277H22.0341V16.2842H25.1884V17.9004H22.0341V21.4324H20.041V12.6479H25.4104V14.277Z"/>
   													</svg>
   													<?php echo esc_html__('Скачать', 'na5ku'); ?>
   												</span>
                                                <?php if (get_sub_field('opt_serv_example_work_file_mb')) : ?>
                                                    <span class="examples_list_slider_item_btn_right"><?php the_sub_field('opt_serv_example_work_file_mb'); ?></span>
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>

                        <?php endif; ?>
                    </div>
                </div>

                <div class="examples_tabs_content" id="tab_2">
                    <div class="examples_last_works">

                        <div class="examples_last_works_head">
                            <div class="examples_last_works_col_1"><?php echo esc_html__('ДАТА', 'na5ku'); ?></div>
                            <div class="examples_last_works_col_2"><?php echo esc_html__('НОМЕР', 'na5ku'); ?></div>
                            <div class="examples_last_works_col_3"><?php echo esc_html__('ТЕМА', 'na5ku'); ?></div>
                            <div class="examples_last_works_col_4"><?php echo esc_html__('ТИП', 'na5ku'); ?></div>
                        </div>
<!--                        --><?php //if (have_rows('opt_serv_last_work', 'option')) : ?>
<!--                            --><?php //while (have_rows('opt_serv_last_work', 'option')) : the_row(); ?>
<!--                                <div class="examples_last_works_row">-->
<!--                                    <div class="examples_last_works_col_1">--><?php //the_sub_field('opt_serv_last_work_date'); ?><!--</div>-->
<!--                                    <div class="examples_last_works_col_2">--><?php //the_sub_field('opt_serv_last_work_num'); ?><!--</div>-->
<!--                                    <div class="examples_last_works_col_3">--><?php //the_sub_field('opt_serv_last_work_title'); ?><!--</div>-->
<!--                                    <div class="examples_last_works_col_4">--><?php //the_sub_field('opt_serv_last_work_type'); ?><!--</div>-->
<!--                                </div>-->
<!--                            --><?php //endwhile; ?>
<!--                        --><?php //endif; ?>
                    </div>
                </div>

                <div class="examples_tabs_content" id="tab_3">
                    <div class="examples_tab_reviews">
                        <div class="examples_tab_reviews_slider" id="examples_tab_reviews_slider">
                            <?php if (have_rows('opt_serv_reviews', 'option')) : ?>
                                <?php while (have_rows('opt_serv_reviews', 'option')) : the_row(); ?>
                                    <div class="examples_tab_reviews_item_wrap">
                                        <div class="examples_tab_reviews_item">
                                            <?php if (get_sub_field('opt_serv_reviews_image')) : ?>
                                                <img src="<?php the_sub_field('opt_serv_reviews_image'); ?>"
                                                     alt="<?php the_sub_field('opt_serv_reviews_title'); ?>"
                                                     class="examples_tab_reviews_item_img">
                                            <?php endif ?>
                                            <div class="examples_tab_reviews_item_content">
                                                <div class="examples_tab_reviews_item_title"><?php the_sub_field('opt_serv_reviews_title'); ?></div>
                                                <div class="examples_tab_reviews_item_text"><?php the_sub_field('opt_serv_reviews_descr'); ?></div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
