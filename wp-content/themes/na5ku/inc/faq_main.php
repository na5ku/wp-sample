	<?php if ( get_field( 'home_faq_title_6', 'option' ) ) : ?>
		<section class="faq_section_wrap">
			<div class="container">
				<div class="faq_section">
					<div class="page_title white">
						<span><?php the_field( 'home_faq_title_6', 'option' ); ?></span>
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="<?php the_field( 'home_faq_title_6', 'option' ); ?>">
					</div>
					<div class="faq_accordion">
						<?php if ( have_rows( 'home_faq_list', 'option' ) ) : ?>
							<?php while ( have_rows( 'home_faq_list', 'option' ) ) : the_row(); ?>
								<div class="faq_accordion_item">
									<div class="faq_accordion_item_head">
										<div class="faq_accordion_item_head_num_wrap">
											<div class="faq_accordion_item_head_num"><?php echo get_row_index() ; ?></div>
										</div>
										<div class="faq_accordion_item_head_title">
											<span><?php the_sub_field( 'home_faq_quest' ); ?></span>
											<div class="faq_accordion_item_head_title_symbol"></div>
										</div>
									</div>
									<div class="faq_accordion_item_content"><?php the_sub_field( 'home_faq_answer' ); ?></div>
								</div>

							<?php endwhile; ?>

						<?php endif; ?>


					</div>
					<a href="<?php the_field( 'home_faq_link_btn_6', 'option' ); ?>" class="faq_all_questions_btn blue_btn"><?php the_field( 'home_faq_btn_6', 'option' ); ?></a>
				</div>
			</div>
		</section>
	<?php endif; ?>