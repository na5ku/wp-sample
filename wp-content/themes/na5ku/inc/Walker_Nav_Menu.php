<?php
class Header_Walker_Nav_Menu extends Walker_Nav_Menu {

  public function start_lvl( &$output, $depth = 0, $args = null ) {
    $block = isset( $args->bem_block ) ? $args->bem_block : explode( ' ', $args->menu_class );
    $block = is_array( $block ) ? $block[0] : $block;
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent = str_repeat( $t, $depth );

    $wrap_sub = '';
    $class_sub = '';
    if($depth+1 == 1){
      $wrap_sub = '<div class="top_submenu_one">';
      $class_sub = 'submenu_one_list';
    } else if($depth+1 == 2){
      $wrap_sub = '<div class="top_submenu_two">';
      $class_sub = 'submenu_two_list';    
    }

    // sub menu classes
    $submenu_classes = array( $block . '__sub-menu', $block . '__sub-menu--level-' . ( $depth + 1 ) );
    $submenu_classes = implode( '  ', array_filter( $submenu_classes ) );

    $output .= "{$n}{$indent}{$wrap_sub}<ul class=\"$submenu_classes $class_sub\">{$n}";
  }

  public function end_lvl( &$output, $depth = 0, $args = null ) {
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }

    $wrap_sub = '';
    $class_sub = '';
    if($depth+1 == 1){
      $wrap_sub = '</div>';
    } else if($depth+1 == 2){
      $wrap_sub = '</div>';  
    }

    $indent  = str_repeat( $t, $depth );
    $output .= "$indent</ul>{$wrap_sub}{$n}";
  }

  public function start_el( &$output, $item, $depth = 0, $args = null, $id = 0 ) {
    $block = isset( $args->bem_block ) ? $args->bem_block : explode( ' ', $args->menu_class );
    $block = is_array( $block ) ? $block[0] : $block;
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

    // list item
    $item_classes = array( $block . '__item' );
    if ( $item->current ) {
      $item_classes[] = $block . '__item--active';
    }
    if ( $item->current_item_ancestor ) {
      $item_classes[] = $block . '__item--active-ancestor';
    }
    if ( $item->current_item_parent ) {
      $item_classes[] = $block . '__item--active-parent';
    }
    $classes = get_post_meta( $item->ID, '_menu_item_classes' );
    foreach ( $classes[0] as $class ) {
      if ( $class ) {
        $item_classes[] = $block . '__item--' . $class;
      }
    }
    if ( is_single() ) {
      $category = get_the_category();
      $cat_id = $category[0]->cat_ID;
      $ancestors = get_ancestors( $cat_id, 'category' );
      if ( in_array( $item->object_id, $ancestors ) ) {
        $item_classes[] = $block . '__item--active-ancestor';
      }
    }
    $item_classes = implode( '  ', array_filter( $item_classes ) );

    $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

    $output .= $indent . '<li class="' . $item_classes .'">';

    // link attributes
    $atts           = array();
    $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
    $atts['target'] = ! empty( $item->target ) ? $item->target : '';
    if ( '_blank' === $item->target && empty( $item->xfn ) ) {
      $atts['rel'] = 'noopener noreferrer';
    } else {
      $atts['rel'] = $item->xfn;
    }
    $atts['href']         = ! empty( $item->url ) ? $item->url : '';
    $atts['aria-current'] = $item->current ? 'page' : '';

    if ( $item->current ) $atts['href'] = '';

    $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

    $attributes = '';
    foreach ( $atts as $attr => $value ) {
      if ( is_scalar( $value ) && '' !== $value && false !== $value ) {
        $value       = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
        $attributes .= ' ' . $attr . '="' . $value . '"';
      }
    }

    $title = apply_filters( 'the_title', $item->title, $item->ID );
    $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

    $svg = ($depth+1 == 2) ? '<svg width="6" height="10" viewBox="0 0 6 10" >
                      <path d="M3.96335 4.99789L0.222376 8.73902C0.119368 8.84179 0.0627021 8.97919 0.0627021 9.12569C0.0627021 9.27228 0.119369 9.40959 0.222376 9.51252L0.550181 9.84016C0.653027 9.94325 0.790506 10 0.937011 10C1.08351 10 1.22083 9.94325 1.32376 9.84016L5.77799 5.38602C5.88132 5.28276 5.9379 5.1448 5.9375 4.99813C5.9379 4.85081 5.8814 4.71301 5.77799 4.60967L1.3279 0.159838C1.22498 0.0567488 1.08766 4.23986e-07 0.941075 4.36801e-07C0.794571 4.49609e-07 0.657254 0.0567489 0.554246 0.159838L0.226522 0.487481C0.01327 0.700733 0.0132701 1.04789 0.226522 1.26106L3.96335 4.99789Z"/>
                    </svg>' : '';

    // link
    $item_output = $args->before;
    $item_output .= '<a class="' . $block . '__link"'. $attributes .'>';
    $item_output .= $args->link_before . $title . $args->link_after;
    $item_output .= '</a>'.$svg;
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }

  public function end_el( &$output, $item, $depth = 0, $args = null ) {
    $block = isset( $args->bem_block ) ? $args->bem_block : explode( ' ', $args->menu_class );
    $block = is_array( $block ) ? $block[0] : $block;
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $output .= "</li>{$n}";
  }

}

class Mobile_Walker_Nav_Menu extends Walker_Nav_Menu {

  public function start_lvl( &$output, $depth = 0, $args = null ) {
    $block = isset( $args->bem_block ) ? $args->bem_block : explode( ' ', $args->menu_class );
    $block = is_array( $block ) ? $block[0] : $block;
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent = str_repeat( $t, $depth );

    $wrap_sub = '';
    $class_sub = '';
    if($depth+1 == 1){
      $wrap_sub = '<div class="mobile_header_sub_nav">';
      $class_sub = 'mobile_header_sub_list';
    } else if($depth+1 == 2){
      $wrap_sub = '<div class="mobile_header_sub_nav_two">';
      $class_sub = 'mobile_header_sub_list_two';    
    }

    // sub menu classes
    $submenu_classes = array( $block . '__sub-menu', $block . '__sub-menu--level-' . ( $depth + 1 ) );
    $submenu_classes = implode( '  ', array_filter( $submenu_classes ) );

    $output .= "{$n}{$indent}{$wrap_sub}<ul class=\"$submenu_classes $class_sub\">{$n}";
  }

  public function end_lvl( &$output, $depth = 0, $args = null ) {
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }

    $wrap_sub = '';
    $class_sub = '';
    if($depth+1 == 1){
      $wrap_sub = '</div>';
    } else if($depth+1 == 2){
      $wrap_sub = '</div>';  
    }

    $indent  = str_repeat( $t, $depth );
    $output .= "$indent</ul>{$wrap_sub}{$n}";
  }

  public function start_el( &$output, $item, $depth = 0, $args = null, $id = 0 ) {
    $block = isset( $args->bem_block ) ? $args->bem_block : explode( ' ', $args->menu_class );
    $block = is_array( $block ) ? $block[0] : $block;
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

    // list item
    $item_classes = array( $block . '__item' );
    if ( $item->current ) {
      $item_classes[] = $block . '__item--active';
    }
    if ( $item->current_item_ancestor ) {
      $item_classes[] = $block . '__item--active-ancestor';
    }
    if ( $item->current_item_parent ) {
      $item_classes[] = $block . '__item--active-parent';
    }
    $classes = get_post_meta( $item->ID, '_menu_item_classes' );
    foreach ( $classes[0] as $class ) {
      if ( $class ) {
        $item_classes[] = $block . '__item--' . $class;
      }
    }
    if ( is_single() ) {
      $category = get_the_category();
      $cat_id = $category[0]->cat_ID;
      $ancestors = get_ancestors( $cat_id, 'category' );
      if ( in_array( $item->object_id, $ancestors ) ) {
        $item_classes[] = $block . '__item--active-ancestor';
      }
    }
    $item_classes = implode( '  ', array_filter( $item_classes ) );

    $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

    $output .= $indent . '<li class="' . $item_classes .'">';

    // link attributes
    $atts           = array();
    $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
    $atts['target'] = ! empty( $item->target ) ? $item->target : '';
    if ( '_blank' === $item->target && empty( $item->xfn ) ) {
      $atts['rel'] = 'noopener noreferrer';
    } else {
      $atts['rel'] = $item->xfn;
    }
    $atts['href']         = ! empty( $item->url ) ? $item->url : '';
    $atts['aria-current'] = $item->current ? 'page' : '';

    if ( $item->current ) $atts['href'] = '';

    $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

    $attributes = '';
    foreach ( $atts as $attr => $value ) {
      if ( is_scalar( $value ) && '' !== $value && false !== $value ) {
        $value       = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
        $attributes .= ' ' . $attr . '="' . $value . '"';
      }
    }

    $title = apply_filters( 'the_title', $item->title, $item->ID );
    $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

    $svg = ($depth+1 == 2) ? '<svg width="6" height="10" viewBox="0 0 6 10" >
                      <path d="M3.96335 4.99789L0.222376 8.73902C0.119368 8.84179 0.0627021 8.97919 0.0627021 9.12569C0.0627021 9.27228 0.119369 9.40959 0.222376 9.51252L0.550181 9.84016C0.653027 9.94325 0.790506 10 0.937011 10C1.08351 10 1.22083 9.94325 1.32376 9.84016L5.77799 5.38602C5.88132 5.28276 5.9379 5.1448 5.9375 4.99813C5.9379 4.85081 5.8814 4.71301 5.77799 4.60967L1.3279 0.159838C1.22498 0.0567488 1.08766 4.23986e-07 0.941075 4.36801e-07C0.794571 4.49609e-07 0.657254 0.0567489 0.554246 0.159838L0.226522 0.487481C0.01327 0.700733 0.0132701 1.04789 0.226522 1.26106L3.96335 4.99789Z"/>
                    </svg>' : '';

    // link
    $item_output = $args->before;
    $item_output .= '<a class="' . $block . '__link"'. $attributes .'>';
    $item_output .= $args->link_before . $title . $args->link_after;
    $item_output .= '</a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }

  public function end_el( &$output, $item, $depth = 0, $args = null ) {
    $block = isset( $args->bem_block ) ? $args->bem_block : explode( ' ', $args->menu_class );
    $block = is_array( $block ) ? $block[0] : $block;
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $output .= "</li>{$n}";
  }

}