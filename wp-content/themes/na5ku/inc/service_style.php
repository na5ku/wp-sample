<style>
<?php if ( get_field( 'opt_serv_first_img_pc', 'option' ) ) : ?>
	.page_wrap .indiv_first_section_wrap{
		background-image: url(<?php the_field( 'opt_serv_first_img_pc', 'option' ); ?>);
	}
<?php endif ?>

<?php if ( get_field( 'opt_serv_adv_image_pc', 'option' ) ) : ?>
	.page_wrap .indiv_features_section_wrap{
		background-image: url(<?php the_field( 'opt_serv_adv_image_pc', 'option' ); ?>);
	}
<?php endif ?>

<?php if ( get_field( 'serv_faq_image_pc') ) : ?>
	.page_wrap .faq_section_wrap{
		background-image: url(<?php the_field( 'serv_faq_image_pc'); ?>);
	}
<?php endif ?>

<?php if ( get_field( 'serv_page_seo_image_pc') ) : ?>
	.page_wrap .how_to_order_section_wrap{
		background-image: url(<?php the_field( 'serv_page_seo_image_pc' ); ?>);
	}
<?php endif ?>

@media screen and (max-width: 767px){
	<?php if ( get_field( 'opt_serv_first_img_mob', 'option' ) ) : ?>
	.page_wrap .indiv_first_section_wrap{
		background-image: url(<?php the_field( 'opt_serv_first_img_mob', 'option' ); ?>);
	}
	<?php endif ?>
	<?php if ( get_field( 'opt_serv_adv_image_mob', 'option' ) ) : ?>
	.page_wrap .indiv_features_section_wrap{
		background-image: url(<?php the_field( 'opt_serv_adv_image_mob', 'option' ); ?>);
	}
	<?php endif ?>
	<?php if ( get_field( 'serv_faq_image_mob') ) : ?>
	.page_wrap .faq_section_wrap{
		background-image: url(<?php the_field( 'serv_faq_image_mob'); ?>);
	}
	<?php endif ?>
	<?php if ( get_field( 'serv_page_seo_image_mob') ) : ?>
	.page_wrap .how_to_order_section_wrap{
		background-image: url(<?php the_field( 'serv_page_seo_image_mob'); ?>);
	}
	<?php endif ?>
}

</style>