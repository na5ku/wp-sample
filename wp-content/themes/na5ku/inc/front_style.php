<style>
<?php if ( get_field( 'home_image_1', 'option' ) ) : ?>
	.page_wrap .main_first_section_wrap{
		background-image: url(<?php the_field( 'home_image_1', 'option' ); ?>);
	}
<?php endif ?>

<?php if ( get_field( 'home_image_2', 'option' ) ) : ?>
	.page_wrap .we_write_section_wrap{
		background-image: url(<?php the_field( 'home_image_2', 'option' ); ?>);
	}
<?php endif ?>

<?php if ( get_field( 'home_faq_image_6', 'option' ) ) : ?>
	.page_wrap .faq_section_wrap{
		background-image: url(<?php the_field( 'home_faq_image_6', 'option' ); ?>);
	}
<?php endif ?>

<?php if ( get_field( 'home_seo_image_8', 'option' ) ) : ?>
	.page_wrap .how_to_order_section_wrap{
		background-image: url(<?php the_field( 'home_seo_image_8', 'option' ); ?>);
	}
<?php endif ?>

@media screen and (max-width: 767px){
	<?php if ( get_field( 'home_image_1_mob', 'option' ) ) : ?>
	.page_wrap .main_first_section_wrap{
		background-image: url(<?php the_field( 'home_image_1_mob', 'option' ); ?>);
	}
	<?php endif ?>
	<?php if ( get_field( 'home_image_mob_2', 'option' ) ) : ?>
	.page_wrap .we_write_section_wrap{
		background-image: url(<?php the_field( 'home_image_mob_2', 'option' ); ?>);
	}
	<?php endif ?>
	<?php if ( get_field( 'home_faq_mob_image_6', 'option' ) ) : ?>
	.page_wrap .faq_section_wrap{
		background-image: url(<?php the_field( 'home_faq_mob_image_6', 'option' ); ?>);
	}
	<?php endif ?>
	<?php if ( get_field( 'home_seo_image_8_mob', 'option' ) ) : ?>
	.page_wrap .how_to_order_section_wrap{
		background-image: url(<?php the_field( 'home_seo_image_8_mob', 'option' ); ?>);
	}
	<?php endif ?>
}

</style>