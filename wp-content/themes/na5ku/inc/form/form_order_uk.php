<link rel="stylesheet" href="https://edu-masters.com/form3/css/main.css">

<div class="na5ku-calcForm2">
    <div class="order">
        <div class="row">
            <div class="col-md-5">
                <div class="order__items">
                    <div class="order-item">
                        <div class="order-item__number">1</div>
                        <div class="order-item__circle">
                            <span class="order-item__icon order-icon-1"></span>
                        </div>
                        <h3 class="order-item__title">Строго дотримуємося
                            <span
                                    class="br">вимоги методички, </span>
                            <span
                                    class="br">знаємо всі ГОСТи</span>
                        </h3>
                    </div>
                    <div class="order-item">
                        <div class="order-item__number">2</div>
                        <div class="order-item__circle">
                            <span class="order-item__icon order-icon-2"></span>
                        </div>
                        <h3 class="order-item__title">Всі роботи проходять
                            <span
                                    class="br">перевірку якості</span>
                        </h3>
                    </div>
                    <div class="order-item">
                        <div class="order-item__number">3</div>
                        <div class="order-item__circle">
                            <span class="order-item__icon order-icon-3"></span>
                        </div>
                        <h3 class="order-item__title">Оцінимо вашу заявку
                            <span
                                    class="br">протягом 2 годин</span>
                        </h3>
                    </div>
                    <div class="order-item">
                        <div class="order-item__number">4</div>
                        <div class="order-item__circle">
                            <span class="order-item__icon order-icon-4"></span>
                        </div>
                        <h3 class="order-item__title">Пишемо плани
                            <span class="br">безкоштовно</span>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <h2 class="order__title">Додати замовлення</h2>
                <form action="https://edu-masters.com/partner_form.php" method="post" class="order-form"
                      autocomplete="off">

                    <div class="order-form__group">
                        <label class="order-form__label">Оберіть тип роботи</label>
                        <select class="order-form__input" name="WorkType" required>
                            <option value="0">Тип</option>
                            <option value="21">Автореферат</option>
                            <option value="22">Анотація</option>
                            <option value="23">Аспірантський реферат</option>
                            <option value="1">Бізнес-план</option>
                            <option value="50">Безкоштовний план (диплом)</option>
                            <option value="49">Безкоштовний план (курсова)</option>
                            <option value="8">Відповіді на білети</option>
                            <option value="2" selected>Дипломна робота</option>
                            <option value="47">Дипломна робота (один розділ)</option>
                            <option value="45">Дипломна робота (правки)</option>
                            <option value="3">Дисертація</option>
                            <option value="25">Домашня робота</option>
                            <option value="17">Допомога на екзамені онлайн</option>
                            <option value="16">Есе</option>
                            <option value="4">Задача</option>
                            <option value="9">Звіт з практики</option>
                            <option value="26">Кейс</option>
                            <option value="5">Контрольна робота</option>
                            <option value="15">Креслення</option>
                            <option value="6">Курсова робота</option>
                            <option value="7">Лабороторна робота</option>
                            <option value="19">Магістерська робота</option>
                            <option value="46">Магістерська робота (один розділ)</option>
                            <option value="48">Магістерська робота (правки)</option>
                            <option value="27">Монографія</option>
                            <option value="28">Наукова стаття</option>
                            <option value="29">Підвищення унікальності</option>
                            <option value="10">Переклад</option>
                            <option value="11">Презентація PowerPoint</option>
                            <option value="12">Реферат</option>
                            <option value="32">Рецензія (тільки текст)</option>
                            <option value="31">Статті до дисертації</option>
                            <option value="18">Стаття</option>
                            <option value="13">Твір</option>
                            <option value="30">Тези</option>
                            <option value="14">Тест</option>
                            <option value="24">Щоденник практики</option>
                        </select>
                    </div>
                    <div class="order-form__group">
                        <label class="order-form__label">Виберіть предмет</label>
                        <select class="order-form__input" name="predmet" required>
                            <option value="0">Виберіть предмет</option>
                            <option value="408">1 : C</option>
                            <option value="481">1 С</option>
                            <option value="409">3D-max</option>
                            <option value="410">Access</option>
                            <option value="411">Android</option>
                            <option value="412">Basic</option>
                            <option value="413">BPWin</option>
                            <option value="414">C/C++</option>
                            <option value="415">C#</option>
                            <option value="416">CorelDraw</option>
                            <option value="417">Delphi</option>
                            <option value="338">Electronics Workbench</option>
                            <option value="418">Excel</option>
                            <option value="419">Fortran</option>
                            <option value="420">HTML</option>
                            <option value="421">Java</option>
                            <option value="422">Javascript</option>
                            <option value="423">Linux</option>
                            <option value="202">MathCad</option>
                            <option value="203">MATLAB</option>
                            <option value="424">MySQL</option>
                            <option value="425">Pascal</option>
                            <option value="426">Perl</option>
                            <option value="427">Photoshop</option>
                            <option value="428">PHP</option>
                            <option value="429">PowerPoint презентація</option>
                            <option value="430">Prolog</option>
                            <option value="247">SPSS</option>
                            <option value="303">SPSS</option>
                            <option value="431">Visual Basic</option>
                            <option value="432">Visual C++</option>
                            <option value="433">web - розробки</option>
                            <option value="501">Авіаційна промисловість</option>
                            <option value="502">Автоматика</option>
                            <option value="503">Автомобільна промисловість</option>
                            <option value="69">Агрономія</option>
                            <option value="70">Агрофізика</option>
                            <option value="71">Агрохімія</option>
                            <option value="129">Адміністративне право</option>
                            <option value="339">Акустика</option>
                            <option value="204">Алгебра</option>
                            <option value="93">Аналіз тексту</option>
                            <option value="482">Аналіз господарської діяльності</option>
                            <option value="327">Аналітична хімія</option>
                            <option value="387">Анатомія</option>
                            <option value="460">Англійська</option>
                            <option value="340">Антени та пристрої НВЧ</option>
                            <option value="543">Антикризове управління</option>
                            <option value="94">Антична філософія</option>
                            <option value="461">Арабська</option>
                            <option value="130">Арбітражний процес</option>
                            <option value="72">Археологія</option>
                            <option value="282">Архітектура</option>
                            <option value="434">Асемблер</option>
                            <option value="341">Астрометрія</option>
                            <option value="342">Астрономія</option>
                            <option value="343">Астрофізика</option>
                            <option value="504">АСУ</option>
                            <option value="483">Аудит</option>
                            <option value="43">Аналіз фінансово-господарської діяльності</option>
                            <option value="435">Бази даних</option>
                            <option value="484">Банківська справа</option>
                            <option value="462">Білоруська</option>
                            <option value="505">БЖ</option>
                            <option value="283">БЖД в будівництві</option>
                            <option value="95">Бібліотекознавство</option>
                            <option value="544">Бізнес планування</option>
                            <option value="73">Біогеографія</option>
                            <option value="388">Біологія</option>
                            <option value="344">Біофізика</option>
                            <option value="328">Біохімія</option>
                            <option value="44">Біржова справа</option>
                            <option value="389">Ботаніка</option>
                            <option value="485">Бухгалтерська та податкова звітність</option>
                            <option value="486">Бухгалтерський облік та аудит</option>
                            <option value="487">Бюджетування</option>
                            <option value="390">Валеологія</option>
                            <option value="45">Валютно-кредитні відносини</option>
                            <option value="391">Ветеринарія</option>
                            <option value="506">Вібраційні процеси</option>
                            <option value="248">Віртуальна психологія</option>
                            <option value="284">Водопостачання та водовідведення</option>
                            <option value="229">Сходознавство</option>
                            <option value="74">Вища геодезія</option>
                            <option value="205">Вища математика</option>
                            <option value="206">Обчислювальна математика</option>
                            <option value="166">Обчислювальна механіка</option>
                            <option value="46">ЗЕД - зовнішньоекономічна діяльність</option>
                            <option value="249">Гендерна психологія</option>
                            <option value="304">Гендерна соціологія</option>
                            <option value="392">Генетика</option>
                            <option value="75">Географія</option>
                            <option value="76">Геодезія</option>
                            <option value="77">Геологія</option>
                            <option value="78">Геологія корисних копалин</option>
                            <option value="207">Геометрія</option>
                            <option value="96">Геополітика</option>
                            <option value="79">Геофізика</option>
                            <option value="80">Геохімія</option>
                            <option value="81">Геоекологія</option>
                            <option value="393">Гігієна</option>
                            <option value="345">Гідравліка</option>
                            <option value="285">Гідромеліоративне будівництво</option>
                            <option value="286">Гідротехнічне будівництво</option>
                            <option value="463">Голландська</option>
                            <option value="545">Готельна справа</option>
                            <option value="546">Державне управління</option>
                            <option value="131">Держава та право</option>
                            <option value="132">Цивільне право</option>
                            <option value="464">Грецька</option>
                            <option value="394">Гриби та мікологія</option>
                            <option value="465">Датська</option>
                            <option value="26">Декоративно-прикладне мистецтво</option>
                            <option value="547">Діловодство</option>
                            <option value="488">Гроші та кредит</option>
                            <option value="507">Деревообробка</option>
                            <option value="11">Деталі машин</option>
                            <option value="97">Діалектологія</option>
                            <option value="184">Дидактика</option>
                            <option value="287">Дизайн інтер&#039;єра</option>
                            <option value="208">Дискретна математика</option>
                            <option value="447">Диференційна геометрія</option>
                            <option value="250">Диференціальна психологія</option>
                            <option value="209">Диференційне рівняння</option>
                            <option value="508">Видобувне виробництво</option>
                            <option value="288">Дорожнє будівництво</option>
                            <option value="185">Дошкільна педагогіка</option>
                            <option value="98">Природознавство</option>
                            <option value="12">Залізничне машинобудування</option>
                            <option value="133">Житлове право</option>
                            <option value="99">Журналістика</option>
                            <option value="134">Земельне право</option>
                            <option value="135">Земельні кадастри</option>
                            <option value="395">Зоологія</option>
                            <option value="396">Зоотехнія</option>
                            <option value="466">Іврит</option>
                            <option value="509">Видавнича справа та поліграфія</option>
                            <option value="397">Імунологія</option>
                            <option value="548">Інвестиції</option>
                            <option value="549">Інвестиційний менеджмент</option>
                            <option value="82">Інженерна гіодезія</option>
                            <option value="83">Інженерна геологія</option>
                            <option value="296">Інженерна графіка</option>
                            <option value="251">Інженерна психологія</option>
                            <option value="550">Інноваційний менеджмент</option>
                            <option value="510">Інструментальна промисловість</option>
                            <option value="551">Інтернет маркетинг</option>
                            <option value="436">Інформатика</option>
                            <option value="437">Інформаційна безпека</option>
                            <option value="438">Інформаційні системи</option>
                            <option value="47">Інформаційні системи в економіці</option>
                            <option value="439">Інформаційні технологія</option>
                            <option value="440">Штучний інтелект</option>
                            <option value="27">Мистецтво та культура</option>
                            <option value="28">Мистецтвознавство</option>
                            <option value="467">Іспанська</option>
                            <option value="448">Дослідження операцій</option>
                            <option value="100">Історична географія</option>
                            <option value="230">Історія</option>
                            <option value="231">Історія архітектури</option>
                            <option value="232">Історія балету</option>
                            <option value="136">Історія держави та права</option>
                            <option value="233">Стародавня історія</option>
                            <option value="234">Історія журналістики</option>
                            <option value="235">Зарубіжна історія</option>
                            <option value="236">Історія зарубіжного мистецтва</option>
                            <option value="237">Історія мистецтва</option>
                            <option value="238">Історія культури</option>
                            <option value="239">Світова історія</option>
                            <option value="240">Історія МЕВ</option>
                            <option value="241">Історія Батьківщини</option>
                            <option value="186">Історія педагогіки</option>
                            <option value="242">Історія країн, що розвиваються</option>
                            <option value="243">Історія політичних вчень</option>
                            <option value="244">Історія реклами</option>
                            <option value="245">Історія релігії</option>
                            <option value="246">Історія середніх віків</option>
                            <option value="48">Історія економічної думки</option>
                            <option value="468">Італійська</option>
                            <option value="469">Казахська</option>
                            <option value="167">Квантова механіка</option>
                            <option value="346">Квантова фізика</option>
                            <option value="329">Квантова хімія</option>
                            <option value="29">Кіно та телебачення</option>
                            <option value="470">Китайська</option>
                            <option value="168">Класична механіка</option>
                            <option value="252">Клінічна психологія</option>
                            <option value="253">Когнітивна психологія</option>
                            <option value="330">Колоїдна хімія</option>
                            <option value="552">Комерція</option>
                            <option value="441">Комп&#039;ютерна графіка</option>
                            <option value="442">Комп&#039;ютерні мережі та системи</option>
                            <option value="137">Конституційне право</option>
                            <option value="254">Конфліктологія</option>
                            <option value="138">Корпоративне право</option>
                            <option value="84">Краєзнавство</option>
                            <option value="139">Криміналістика</option>
                            <option value="140">Кримінологія</option>
                            <option value="443">Криптографія</option>
                            <option value="30">Культурно-відпочинкова діяльність</option>
                            <option value="101">Культурологія</option>
                            <option value="289">Ландшафтний дизайн</option>
                            <option value="471">Латинська</option>
                            <option value="511">Легка промисловість</option>
                            <option value="102">Лексикологія</option>
                            <option value="512">Лісове господарство</option>
                            <option value="13">Промислове машинобудування</option>
                            <option value="103">Лінгвістика</option>
                            <option value="104">Лінгвістика тексту</option>
                            <option value="210">Лінійна алгебра</option>
                            <option value="211">Лінійне програмування</option>
                            <option value="105">Зарубіжна література</option>
                            <option value="106">Українська література</option>
                            <option value="107">Літературознавство</option>
                            <option value="212">Логіка</option>
                            <option value="108">Гуманітарна логіка</option>
                            <option value="181">Логістика</option>
                            <option value="187">Логопедія</option>
                            <option value="49">Макроекономіка</option>
                            <option value="553">Маркетинг</option>
                            <option value="554">Маркетинг закупівель</option>
                            <option value="555">Маркетинг продажу</option>
                            <option value="85">Маркшейдерська справа</option>
                            <option value="556">Мас-медіа</option>
                            <option value="449">Математичні методи в економіці</option>
                            <option value="255">Математичні методи в психології</option>
                            <option value="213">Математична логіка</option>
                            <option value="450">Математична статистика</option>
                            <option value="347">Математична фізика</option>
                            <option value="331">Математична хімія</option>
                            <option value="214">Математичні методи</option>
                            <option value="305">Математичні методи в соціології</option>
                            <option value="50">Математичні моделі в економіці</option>
                            <option value="215">Математичний аналіз</option>
                            <option value="216">Математичне моделювання</option>
                            <option value="217">Математичне моделювання</option>
                            <option value="513">Матеріалознавство</option>
                            <option value="514">Матеріалознавство легкої промисловості</option>
                            <option value="451">Матпро</option>
                            <option value="398">Медицина</option>
                            <option value="256">Медична психологія</option>
                            <option value="141">Міжнародне право</option>
                            <option value="109">Міжнародні відносини</option>
                            <option value="557">Міжнародний маркетинг</option>
                            <option value="558">Міжнародний менеджмент</option>
                            <option value="559">Менеджмент</option>
                            <option value="560">Менеджмент організації</option>
                            <option value="561">Менеджмент соціально-культурної сфери</option>
                            <option value="515">Металообробка</option>
                            <option value="516">Металургія кольорових металів</option>
                            <option value="110">Метафізика (онтологія)</option>
                            <option value="188">Методика викладання</option>
                            <option value="189">Методика викладання англійської мови</option>
                            <option value="190">Методика викладання технічних дисциплін</option>
                            <option value="191">Методика викладання економічних дисциплін</option>
                            <option value="348">Метрологія</option>
                            <option value="290">Механізація будівництва</option>
                            <option value="169">Механіка</option>
                            <option value="170">Механіка суцільних середовищ</option>
                            <option value="399">Мікробіологія</option>
                            <option value="86">Мікроструктурна геологія</option>
                            <option value="51">Мікроекономіка</option>
                            <option value="349">Мікроелектроніка</option>
                            <option value="31">Світова культура моди</option>
                            <option value="32">Світова художня культура</option>
                            <option value="52">Міжнародна економіка</option>
                            <option value="517">Моделювання в металургії</option>
                            <option value="489">МСФЗ</option>
                            <option value="33">Музика</option>
                            <option value="142">Муніципальне право</option>
                            <option value="562">Муніципальне управління</option>
                            <option value="53">МЕВ</option>
                            <option value="490">Податки</option>
                            <option value="143">Податкове право</option>
                            <option value="34">Народне мистецтво</option>
                            <option value="171">Спадкова механіка</option>
                            <option value="297">Нарисна геометрія</option>
                            <option value="172">Небесна механіка</option>
                            <option value="173">Неголомна механіка</option>
                            <option value="257">Нейропсихологія</option>
                            <option value="350">Нелінійна динаміка</option>
                            <option value="472">Німецька</option>
                            <option value="332">Неорганічна хімія</option>
                            <option value="14">Нафтогазове машинобудування</option>
                            <option value="518">Обладнання легкої промисловості</option>
                            <option value="519">Обладнання харчової промисловості</option>
                            <option value="520">Обробне виробництво</option>
                            <option value="258">Загальна психологія</option>
                            <option value="111">Громадська думка</option>
                            <option value="144">ОВС - Органи внутрішніх справ</option>
                            <option value="444">ООП</option>
                            <option value="351">Оптика</option>
                            <option value="521">Організація громадського харчування</option>
                            <option value="563">Організація виробництва та праці</option>
                            <option value="445">Організація ЕВМ</option>
                            <option value="333">Органічна хімія</option>
                            <option value="400">Ортопедія</option>
                            <option value="446">Основи програмування</option>
                            <option value="522">Основи теорії зварювання та різання металів</option>
                            <option value="352">ОТЛ</option>
                            <option value="87">Охорона довкілля</option>
                            <option value="523">Охорона праці та техніка безпеки</option>
                            <option value="54">Оцінка вартості землі</option>
                            <option value="55">Оцінка вартості нерухомості</option>
                            <option value="401">Палеонтологія</option>
                            <option value="524">Перукарська справа</option>
                            <option value="402">Патологія</option>
                            <option value="259">Патопсихологія</option>
                            <option value="291">Промислове та цивільне будівництво</option>
                            <option value="192">Педагогіка</option>
                            <option value="193">Педагогічна інноватика</option>
                            <option value="260">Педагогічна психологія</option>
                            <option value="564">Планування та прогнозування</option>
                            <option value="15">Підйомно-транспортні механізми</option>
                            <option value="112">Політична географія</option>
                            <option value="35">Політична культура</option>
                            <option value="113">Політичні інститути</option>
                            <option value="145">Політичне право</option>
                            <option value="114">Політологія</option>
                            <option value="473">Польська</option>
                            <option value="474">Португальська</option>
                            <option value="146">Право</option>
                            <option value="147">Право інтелектуальної власності</option>
                            <option value="148">Право соціального забезпечення</option>
                            <option value="149">Правова статистика</option>
                            <option value="353">Практична гідравліка</option>
                            <option value="565">Підприємництво</option>
                            <option value="16">Приладобудування</option>
                            <option value="115">Прикладна лінгвістика</option>
                            <option value="218">Прикладна математика</option>
                            <option value="174">Прикладна механіка</option>
                            <option value="261">Прикладна психологія</option>
                            <option value="292">Проектування систем вентиляції, опалення</option>
                            <option value="293">Проектно-кошторисна документація</option>
                            <option value="525">Виробничі технології</option>
                            <option value="526">Виробництво металевих виробів</option>
                            <option value="527">Виробництво технологічного обладнання</option>
                            <option value="150">Прокурорський нагляд</option>
                            <option value="528">Промислове виробництво</option>
                            <option value="529">Промисловість побутових приладів та машин</option>
                            <option value="116">Професійна етика</option>
                            <option value="262">Психоаналіз</option>
                            <option value="263">Психодіагностика</option>
                            <option value="264">Психолінгвістика</option>
                            <option value="265">Психологія</option>
                            <option value="266">Психологія сприйняття</option>
                            <option value="267">Психологія здоров&#039;я</option>
                            <option value="268">Психологія особистості</option>
                            <option value="269">Психологія розвитку</option>
                            <option value="270">Психологія спорту</option>
                            <option value="271">Психологія праці</option>
                            <option value="272">Психопатологія</option>
                            <option value="273">Психосоматика</option>
                            <option value="274">Психотерапія</option>
                            <option value="275">Психофізіологія</option>
                            <option value="530">Радіотехнічна та електрона промисловість</option>
                            <option value="354">Радіофізика</option>
                            <option value="355">Радіоелектроніка</option>
                            <option value="531">Ракетно-космічна галузь</option>
                            <option value="566">Реклама та PR</option>
                            <option value="117">Релігієзнавство</option>
                            <option value="175">Релятивістська механіка</option>
                            <option value="567">Ресторанний бізнес</option>
                            <option value="151">Римське право</option>
                            <option value="118">Риторика</option>
                            <option value="356">Робототехніка</option>
                            <option value="119">Українська мова</option>
                            <option value="491">Ринок цінних паперів</option>
                            <option value="568">Зв&#039;язки з громадськістю</option>
                            <option value="532">Сільське господарство</option>
                            <option value="17">Сільськогосподарське машинобудування</option>
                            <option value="152">Сімейне право</option>
                            <option value="403">Сестринська справа</option>
                            <option value="36">Соціально-культурна діяльність</option>
                            <option value="182">Складська логістика</option>
                            <option value="176">Опір матеріалів</option>
                            <option value="306">Соціальна антропологія</option>
                            <option value="194">Соціальна педагогіка</option>
                            <option value="276">Соціальна психологія</option>
                            <option value="307">Соціальна робота</option>
                            <option value="308">Соціальна статистика</option>
                            <option value="88">Соціально-економічна географія</option>
                            <option value="37">Соціально-культурне проектування</option>
                            <option value="120">Соціолінгвістика</option>
                            <option value="309">Соціологія</option>
                            <option value="310">Соціологія знань</option>
                            <option value="311">Соціологія комунікацій</option>
                            <option value="312">Соціологія культури</option>
                            <option value="313">Соціологія особистості</option>
                            <option value="314">Соціологія молоді</option>
                            <option value="315">Соціологія освіти</option>
                            <option value="316">Соціологія організацій</option>
                            <option value="317">Соціологія політики</option>
                            <option value="318">Соціологія права</option>
                            <option value="319">Соціологія релігії</option>
                            <option value="320">Соціологія ризику</option>
                            <option value="321">Соціологія села</option>
                            <option value="322">Соціологія сім&#039;ї</option>
                            <option value="323">Соціологія ЗМІ</option>
                            <option value="324">Соціологія праці</option>
                            <option value="195">Соціальна педагогіка (дефектологія)</option>
                            <option value="302">Спеціальна підготовка</option>
                            <option value="277">Спеціальна психологія</option>
                            <option value="196">Порівняльна педагогіка</option>
                            <option value="278">Порівняльна психологія</option>
                            <option value="533">Стандартизація та сертифікація</option>
                            <option value="534">Верстатобудування</option>
                            <option value="452">Статистика та теорія ймовірності</option>
                            <option value="177">Статична механіка</option>
                            <option value="357">Статична фізика</option>
                            <option value="121">Стилістика</option>
                            <option value="569">Стратегічний маркетинг</option>
                            <option value="570">Стратегічний менеджмент</option>
                            <option value="56">Страхування</option>
                            <option value="453">Страхова математика</option>
                            <option value="178">Будівельна механіка</option>
                            <option value="18">Будівельне та комунальне машинобудування</option>
                            <option value="294">Будівництво</option>
                            <option value="153">Судова бухгалтерія</option>
                            <option value="154">Судова експертиза</option>
                            <option value="19">Суднобудування</option>
                            <option value="197">Сурдопедагогіка</option>
                            <option value="358">Схемотехніка</option>
                            <option value="298">Схеми та креслення в Visio</option>
                            <option value="57">Митна справа</option>
                            <option value="155">Митне право</option>
                            <option value="535">ТАУ</option>
                            <option value="454">Теорія ймовірності та мат. статистика</option>
                            <option value="38">Театр</option>
                            <option value="359">Телекомунікації</option>
                            <option value="179">Теоретична механіка</option>
                            <option value="360">Теоретичні основи гідравліки</option>
                            <option value="219">Теорія алгоритмів</option>
                            <option value="198">Теорія виховання</option>
                            <option value="156">Теорія держави та права</option>
                            <option value="455">Теорія графів</option>
                            <option value="220">Теорія ігор</option>
                            <option value="221">Теорія інформації</option>
                            <option value="361">Теорія коливань</option>
                            <option value="536">Теорія металургійних процесів</option>
                            <option value="222">Теорія множин</option>
                            <option value="223">Теорія оптимізації</option>
                            <option value="571">Теорія організації</option>
                            <option value="362">Теорія пластичності</option>
                            <option value="572">Теорія прийняття рішень</option>
                            <option value="573">Теорія прийняття управлінських рішень</option>
                            <option value="574">Теорія управління</option>
                            <option value="363">Теорія пружності</option>
                            <option value="364">Теорія стійкості та катастроф</option>
                            <option value="365">Теплотехніка</option>
                            <option value="366">Термодинаміка</option>
                            <option value="367">Технічна фізика</option>
                            <option value="20">Технічне обслуговування та ремонт автомобілів</option>
                            <option value="21">Технологія машинобудування</option>
                            <option value="537">Технологія харчової промисловості</option>
                            <option value="199">Тифлопедагогіка</option>
                            <option value="22">Теорія машин та механізмів</option>
                            <option value="58">Товарознавство непродовольчих товарів</option>
                            <option value="59">Товарознавство продовольчих товарів</option>
                            <option value="89">Топографія</option>
                            <option value="224">Топологія</option>
                            <option value="225">Топологія</option>
                            <option value="368">ТОЕ</option>
                            <option value="23">Тракторобудування</option>
                            <option value="24">Транспорт</option>
                            <option value="183">Транспортна логістика</option>
                            <option value="157">Транспортне право</option>
                            <option value="295">Транспортне будівництво</option>
                            <option value="158">Трудове право</option>
                            <option value="575">Туризм</option>
                            <option value="226">Теорія функцій комплексної змінної</option>
                            <option value="369">ТЕЦ</option>
                            <option value="159">Кримінальне право</option>
                            <option value="475">Українська</option>
                            <option value="576">Управління якістю</option>
                            <option value="577">Управління персоналом</option>
                            <option value="578">Управління проектами</option>
                            <option value="579">Управління ризиками</option>
                            <option value="492">Облік в АПК</option>
                            <option value="493">Облік в банківській сфері</option>
                            <option value="494">Облік в промисловості</option>
                            <option value="495">Облік в будівництві</option>
                            <option value="496">Облік в сфері обслуговування</option>
                            <option value="497">Облік в торгівлі</option>
                            <option value="334">Фармацевтична хімія</option>
                            <option value="370">Фізика</option>
                            <option value="371">Фізика атмосфери</option>
                            <option value="372">Фізика високих енергій</option>
                            <option value="373">Фізика конденсованих середовищ</option>
                            <option value="374">Фізика плазми</option>
                            <option value="375">Фізика твердого тіла атомів молекул нано</option>
                            <option value="376">Фізика елементарних часток</option>
                            <option value="404">Фізіологія</option>
                            <option value="90">Фізична географія</option>
                            <option value="335">Фізична хімія</option>
                            <option value="377">Фізичні основи електротехніки</option>
                            <option value="200">Фізична культура та спорт</option>
                            <option value="122">Філологія</option>
                            <option value="123">Філософія</option>
                            <option value="160">Філософія права</option>
                            <option value="498">Фінансова математика</option>
                            <option value="161">Фінансове право</option>
                            <option value="580">Фінансовий менеджмент</option>
                            <option value="499">Фінанси</option>
                            <option value="476">Фінська</option>
                            <option value="39">Фольклор</option>
                            <option value="40">Фотомистецтво</option>
                            <option value="477">Французька</option>
                            <option value="227">Функціональний аналіз</option>
                            <option value="336">Хімічна термодинаміка</option>
                            <option value="378">Хімічна фізика</option>
                            <option value="25">Хімічне машинобудування</option>
                            <option value="337">Хімія</option>
                            <option value="405">Хірургія</option>
                            <option value="162">Господарське право</option>
                            <option value="538">Холодильне обладнання</option>
                            <option value="41">Хореографія</option>
                            <option value="500">Ціноутворення</option>
                            <option value="406">Цитологія</option>
                            <option value="379">Цифрова обробка сигналу</option>
                            <option value="299">Креслення в AutoCad</option>
                            <option value="300">Креслення в SolidWorks</option>
                            <option value="301">Креслення в КОМПАС</option>
                            <option value="478">Чеська</option>
                            <option value="456">Чисельні методи</option>
                            <option value="479">Шведська</option>
                            <option value="201">Школознавство</option>
                            <option value="228">Шкільна математика</option>
                            <option value="163">Екологічне право</option>
                            <option value="91">Екологія</option>
                            <option value="457">Економетрика</option>
                            <option value="60">Економіка</option>
                            <option value="61">Економіка АПК</option>
                            <option value="62">Економіка будівництва</option>
                            <option value="63">Економіка галузі</option>
                            <option value="64">Економіка підприємства</option>
                            <option value="65">Економіка природокористування</option>
                            <option value="66">Економіка фармації</option>
                            <option value="92">Економічна географія</option>
                            <option value="279">Економічна психологія</option>
                            <option value="67">Економічна соціологія</option>
                            <option value="458">Економічна статистика</option>
                            <option value="68">Економічна теорія</option>
                            <option value="164">Економічне право</option>
                            <option value="325">Екосоціологія</option>
                            <option value="42">Екскурсознавство</option>
                            <option value="280">Експериментальна психологія</option>
                            <option value="380">Електродинаміка</option>
                            <option value="180">Електромеханіка</option>
                            <option value="381">Електроніка</option>
                            <option value="382">Електрозв&#039;язок</option>
                            <option value="383">Електропостачання</option>
                            <option value="384">Електротехніка</option>
                            <option value="539">Електротехнічна промисловість</option>
                            <option value="540">Електроенергетика</option>
                            <option value="385">Емісійна електроніка</option>
                            <option value="459">ЕММ</option>
                            <option value="541">Енергетика</option>
                            <option value="542">Енергозбереження</option>
                            <option value="124">Епістемологія</option>
                            <option value="581">Ергономіка</option>
                            <option value="125">Естетика</option>
                            <option value="126">Етика</option>
                            <option value="127">Етнолінгвістика</option>
                            <option value="326">Етносоціологія</option>
                            <option value="407">Етологія</option>
                            <option value="281">Юридична психологія</option>
                            <option value="165">Юриспруденція</option>
                            <option value="386">Ядерна фізика</option>
                            <option value="128">Мовознавство</option>
                            <option value="480">Японська</option>
                        </select>
                    </div>

                    <div class="order-form__group">
                        <label class="order-form__label">Ваша тема</label>
                        <input class="order-form__input" type="text" name="Topic" required
                               placeholder="Тема роботи або заголовок">
                    </div>


                    <div class="row">


                        <div class="col-md-6">
                            <div class="order-form__group">
                                <label class="order-form__label">Термін здачі</label>
                                <input class="order-form__input orderDeadline" type="text" name="srok" required
                                       placeholder="Вкажіть дату">
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="order-form__group">
                                <label class="order-form__label">К-сть сторінок</label>
                                <select class="order-form__input" name="pages">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                    <option value="32">32</option>
                                    <option value="33">33</option>
                                    <option value="34">34</option>
                                    <option value="35">35</option>
                                    <option value="36">36</option>
                                    <option value="37">37</option>
                                    <option value="38">38</option>
                                    <option value="39">39</option>
                                    <option value="40">40</option>
                                    <option value="41">41</option>
                                    <option value="42">42</option>
                                    <option value="43">43</option>
                                    <option value="44">44</option>
                                    <option value="45">45</option>
                                    <option value="46">46</option>
                                    <option value="47">47</option>
                                    <option value="48">48</option>
                                    <option value="49">49</option>
                                    <option value="50">50</option>
                                    <option value="51">51</option>
                                    <option value="52">52</option>
                                    <option value="53">53</option>
                                    <option value="54">54</option>
                                    <option value="55">55</option>
                                    <option value="56">56</option>
                                    <option value="57">57</option>
                                    <option value="58">58</option>
                                    <option value="59">59</option>
                                    <option value="60">60</option>
                                    <option value="61">61</option>
                                    <option value="62">62</option>
                                    <option value="63">63</option>
                                    <option value="64">64</option>
                                    <option value="65">65</option>
                                    <option value="66">66</option>
                                    <option value="67">67</option>
                                    <option value="68">68</option>
                                    <option value="69">69</option>
                                    <option value="70">70</option>
                                    <option value="71">71</option>
                                    <option value="72">72</option>
                                    <option value="73">73</option>
                                    <option value="74">74</option>
                                    <option value="75">75</option>
                                    <option value="76">76</option>
                                    <option value="77">77</option>
                                    <option value="78">78</option>
                                    <option value="79">79</option>
                                    <option value="80">80</option>
                                    <option value="81">81</option>
                                    <option value="82">82</option>
                                    <option value="83">83</option>
                                    <option value="84">84</option>
                                    <option value="85">85</option>
                                    <option value="86">86</option>
                                    <option value="87">87</option>
                                    <option value="88">88</option>
                                    <option value="89">89</option>
                                    <option value="90">90</option>
                                    <option value="91">91</option>
                                    <option value="92">92</option>
                                    <option value="93">93</option>
                                    <option value="94">94</option>
                                    <option value="95">95</option>
                                    <option value="96">96</option>
                                    <option value="97">97</option>
                                    <option value="98">98</option>
                                    <option value="99">99</option>
                                    <option value="100">100</option>
                                    <option value="101">101</option>
                                    <option value="102">102</option>
                                    <option value="103">103</option>
                                    <option value="104">104</option>
                                    <option value="105">105</option>
                                    <option value="106">106</option>
                                    <option value="107">107</option>
                                    <option value="108">108</option>
                                    <option value="109">109</option>
                                    <option value="110">110</option>
                                    <option value="111">111</option>
                                    <option value="112">112</option>
                                    <option value="113">113</option>
                                    <option value="114">114</option>
                                    <option value="115">115</option>
                                    <option value="116">116</option>
                                    <option value="117">117</option>
                                    <option value="118">118</option>
                                    <option value="119">119</option>
                                    <option value="120">120</option>
                                    <option value="121">121</option>
                                    <option value="122">122</option>
                                    <option value="123">123</option>
                                    <option value="124">124</option>
                                    <option value="125">125</option>
                                    <option value="126">126</option>
                                    <option value="127">127</option>
                                    <option value="128">128</option>
                                    <option value="129">129</option>
                                    <option value="130">130</option>
                                    <option value="131">131</option>
                                    <option value="132">132</option>
                                    <option value="133">133</option>
                                    <option value="134">134</option>
                                    <option value="135">135</option>
                                    <option value="136">136</option>
                                    <option value="137">137</option>
                                    <option value="138">138</option>
                                    <option value="139">139</option>
                                    <option value="140">140</option>
                                    <option value="141">141</option>
                                    <option value="142">142</option>
                                    <option value="143">143</option>
                                    <option value="144">144</option>
                                    <option value="145">145</option>
                                    <option value="146">146</option>
                                    <option value="147">147</option>
                                    <option value="148">148</option>
                                    <option value="149">149</option>
                                    <option value="150">150</option>
                                </select>
                            </div>
                        </div>


                    </div>


                    <div class="order-form__group">
                        <textarea class="order-form__input" name="must_do" rows="5"
                                  placeholder="Коментар (Не обов'язково)"></textarea>
                    </div>


                    <h3 class="order-form__title">
                        <span>Уточніть завдання</span>
                    </h3>
                    <div class="order-form__group">
                        <label class="order-form__label">Ел. пошта</label>
                        <input class="order-form__input" type="email" name="Email" required
                               placeholder="mail@mail.com">
                    </div>


                    <div class="order-form__group">
                        <label class="order-form__label">Телефон</label>
                        <input class="order-form__input phone-mask" type="text" name="Phone"
                               placeholder="+380 (__) ___-____">
                    </div>


                    <input name="ref_id" type="hidden" value="<?php the_field( 'user_id_ref', 'option' ); ?>">
                    <input name="Country" type="hidden" value="1">
                    <div class="order-form__group order-form__group--button">
                        <button class="button button--blue" type="submit">Надіслати заявку на оцінку</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- DATETIME LIBRARY -->
<link rel="stylesheet" href="https://edu-masters.com/form/jquery.datetimepicker.min.css">
<script src="https://edu-masters.com/form/jquery.datetimepicker.full.min.js"></script>
<!-- MASK -->
<script src="https://edu-masters.com/form/jquery.maskedinput.min.js"></script>

<script>
    if (typeof jQuery === "undefined") {
        var scriptElement = document.createElement("script");
        scriptElement.setAttribute("src", "https://edu-masters.com/form/jquery.min.2.js");
        scriptElement.setAttribute("crossorigin", "anonymous");
        scriptElement.onload = function () {
            init();
        };
        document.head.insertAdjacentElement("beforeend", scriptElement);

    }

    document.addEventListener("DOMContentLoaded", function () {
        $('.na5ku-calcForm2 .orderDeadline').datetimepicker({
            timepicker: true,
            format: 'Y/m/d H:i' //another format will be ignored
        });


        //$('.select-input-select2').select2();

        $('.na5ku-calcForm2 .phone-mask').mask('+380 (99) 999-9999');
    }, false);

</script>

