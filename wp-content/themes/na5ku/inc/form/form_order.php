<link rel="stylesheet" href="https://edu-masters.com/form3/css/main.css">

<div class="na5ku-calcForm2">
    <div class="order">
        <div class="row">
            <div class="col-md-5">
                <div class="order__items">
                    <div class="order-item">
                        <div class="order-item__number">1</div>
                        <div class="order-item__circle">
                            <span class="order-item__icon order-icon-1"></span>
                        </div>
                        <h3 class="order-item__title">Строго соблюдаем
                            <span
                                    class="br">требования методички, </span>
                            <span
                                    class="br">знаем все ГОСТы</span>
                        </h3>
                    </div>
                    <div class="order-item">
                        <div class="order-item__number">2</div>
                        <div class="order-item__circle">
                            <span class="order-item__icon order-icon-2"></span>
                        </div>
                        <h3 class="order-item__title">Все работы проходят
                            <span
                                    class="br">проверку качества</span>
                        </h3>
                    </div>
                    <div class="order-item">
                        <div class="order-item__number">3</div>
                        <div class="order-item__circle">
                            <span class="order-item__icon order-icon-3"></span>
                        </div>
                        <h3 class="order-item__title">Оценим вашу заявку
                            <span
                                    class="br">в течение 2 часов</span>
                        </h3>
                    </div>
                    <div class="order-item">
                        <div class="order-item__number">4</div>
                        <div class="order-item__circle">
                            <span class="order-item__icon order-icon-4"></span>
                        </div>
                        <h3 class="order-item__title">Пишем планы
                            <span class="br">бесплатно</span>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <h2 class="order__title">Добавить заказ</h2>
                <form action="https://edu-masters.com/partner_form.php" method="post" class="order-form"
                      autocomplete="off">

                    <div class="order-form__group">
                        <label class="order-form__label">Тип работы</label>
                        <select class="order-form__input" name="WorkType" required>
                            <option value="0">Тип</option>
                            <option value="21">Автореферат</option>
                            <option value="22">Аннотация</option>
                            <option value="23">Аспирантский реферат</option>
                            <option value="50">Бесплатный план (диплом)</option>
                            <option value="49">Бесплатный план (курсовая)</option>
                            <option value="1">Бизнес-план</option>
                            <option value="2" selected>Дипломная работа</option>
                            <option value="47">Дипломная работа (один раздел)</option>
                            <option value="45">Дипломная работа (правки)</option>
                            <option value="3">Диссертация</option>
                            <option value="24">Дневник по практике</option>
                            <option value="25">Домашняя работа</option>
                            <option value="4">Задача</option>
                            <option value="26">Кейс</option>
                            <option value="5">Контрольная работа</option>
                            <option value="6">Курсовая работа</option>
                            <option value="7">Лабораторная работа</option>
                            <option value="19">Магистерская работа</option>
                            <option value="48">Магистерская работа (один раздел)</option>
                            <option value="46">Магистерская работа (правки)</option>
                            <option value="27">Монография</option>
                            <option value="28">Научная статья</option>
                            <option value="8">Ответы на билеты</option>
                            <option value="9">Отчет по практике</option>
                            <option value="10">Перевод</option>
                            <option value="29">Повышение уникальности</option>
                            <option value="11">Презентация PowerPoint</option>
                            <option value="12">Реферат</option>
                            <option value="32">Рецензия (только текст)</option>
                            <option value="17">Решение экзаменов онлайн</option>
                            <option value="13">Сочинение</option>
                            <option value="31">Статьи для диссертации</option>
                            <option value="18">Статья</option>
                            <option value="30">Тезисы</option>
                            <option value="14">Тест</option>
                            <option value="15">Чертеж</option>
                            <option value="16">Эссе</option>
                        </select>
                    </div>
                    <div class="order-form__group">
                        <label class="order-form__label">Предмет:</label>
                        <select class="order-form__input" name="predmet" required>
                            <option value="0">Предмет:</option>
                            <option value="408">1 : C</option>
                            <option value="481">1 с</option>
                            <option value="409">3D-max</option>
                            <option value="410">Access</option>
                            <option value="411">Android</option>
                            <option value="412">Basic</option>
                            <option value="413">BPWin</option>
                            <option value="414">C/C++</option>
                            <option value="415">C#</option>
                            <option value="416">CorelDraw</option>
                            <option value="417">Delphi</option>
                            <option value="338">Electronics Workbench</option>
                            <option value="418">Excel</option>
                            <option value="419">Fortran</option>
                            <option value="420">HTML</option>
                            <option value="421">Java</option>
                            <option value="422">Javascript</option>
                            <option value="423">Linux</option>
                            <option value="202">MathCad</option>
                            <option value="203">MATLAB</option>
                            <option value="424">MySQL</option>
                            <option value="425">Pascal</option>
                            <option value="426">Perl</option>
                            <option value="427">Photoshop</option>
                            <option value="428">PHP</option>
                            <option value="429">PowerPoint презентация</option>
                            <option value="430">Prolog</option>
                            <option value="247">SPSS</option>
                            <option value="303">SPSS</option>
                            <option value="431">Visual Basic</option>
                            <option value="432">Visual C++</option>
                            <option value="433">web - разработки</option>
                            <option value="501">Авиационная промышленность</option>
                            <option value="502">Автоматика</option>
                            <option value="503">Автомобильная промышленность</option>
                            <option value="69">Агрономия</option>
                            <option value="70">Агрофизика</option>
                            <option value="71">Агрохимия</option>
                            <option value="129">Административное право</option>
                            <option value="339">Акустика</option>
                            <option value="204">Алгебра</option>
                            <option value="93">Анализ текста</option>
                            <option value="482">Анализ хоз. деятельности</option>
                            <option value="327">Аналитическая химия</option>
                            <option value="387">Анатомия</option>
                            <option value="460">Английский</option>
                            <option value="340">Антенны и устройства СВЧ</option>
                            <option value="543">Антикризисное управление</option>
                            <option value="94">Античная философия</option>
                            <option value="461">Арабский</option>
                            <option value="130">Арбитражный процесс</option>
                            <option value="72">Археология</option>
                            <option value="282">Архитектура</option>
                            <option value="434">Ассемблер</option>
                            <option value="341">Астрометрия</option>
                            <option value="342">Астрономия</option>
                            <option value="343">Астрофизика</option>
                            <option value="504">АСУ</option>
                            <option value="483">Аудит</option>
                            <option value="43">АФХД</option>
                            <option value="435">Базы данных</option>
                            <option value="484">Банковское дело</option>
                            <option value="462">Белорусский</option>
                            <option value="505">БЖ</option>
                            <option value="283">БЖД в строительстве</option>
                            <option value="95">Библиотековедение</option>
                            <option value="544">Бизнес-планирование</option>
                            <option value="73">Биогеография</option>
                            <option value="388">Биология</option>
                            <option value="344">Биофизика</option>
                            <option value="328">Биохимия</option>
                            <option value="44">Биржевое дело</option>
                            <option value="389">Ботаника</option>
                            <option value="485">Бухгалтерская и налоговая отчетность</option>
                            <option value="486">Бухгалтерский учет и аудит</option>
                            <option value="487">Бюджетирование</option>
                            <option value="390">Валеология</option>
                            <option value="45">Валютные отношения</option>
                            <option value="391">Ветеринария</option>
                            <option value="506">Вибрационные процессы</option>
                            <option value="248">Виртуальная психология</option>
                            <option value="284">Водоснабжение и водоотведение</option>
                            <option value="229">Востоковедение</option>
                            <option value="74">Высшая геодезия</option>
                            <option value="205">Высшая математика</option>
                            <option value="206">Вычислительная математика</option>
                            <option value="166">Вычислительная механика</option>
                            <option value="46">ВЭД – внешнеэкономическая деятельность</option>
                            <option value="249">Гендерная психология</option>
                            <option value="304">Гендерная социология</option>
                            <option value="392">Генетика</option>
                            <option value="75">География</option>
                            <option value="76">Геодезия</option>
                            <option value="77">Геология</option>
                            <option value="78">Геология полезных ископаемых</option>
                            <option value="207">Геометрия</option>
                            <option value="96">Геополитика</option>
                            <option value="79">Геофизика</option>
                            <option value="80">Геохимия</option>
                            <option value="81">Геоэкология</option>
                            <option value="393">Гигиена</option>
                            <option value="345">Гидравлика</option>
                            <option value="285">Гидромелиоративное строительство</option>
                            <option value="286">Гидротехническое строительство</option>
                            <option value="463">Голландский</option>
                            <option value="545">Гостиничное дело</option>
                            <option value="546">Государственное управление</option>
                            <option value="131">Государство и право</option>
                            <option value="132">Гражданское право</option>
                            <option value="464">Греческий</option>
                            <option value="394">Грибы и микология</option>
                            <option value="465">Датский</option>
                            <option value="26">Декоративно-прикладное искусство</option>
                            <option value="547">Делопроизводство</option>
                            <option value="488">Деньги и кредит</option>
                            <option value="507">Деревообработка</option>
                            <option value="11">Детали машин</option>
                            <option value="97">Диалектология</option>
                            <option value="184">Дидактика</option>
                            <option value="287">Дизайн интерьера</option>
                            <option value="208">Дискретная математика</option>
                            <option value="447">Дифференциальная геометрия</option>
                            <option value="250">Дифференциальная психология</option>
                            <option value="209">Дифференциальные уравнения</option>
                            <option value="508">Добывающее производство</option>
                            <option value="288">Дорожное строительство</option>
                            <option value="185">Дошкольная педагогика</option>
                            <option value="98">Естествознание</option>
                            <option value="12">Железнодорожное машиностроение</option>
                            <option value="133">Жилищное право</option>
                            <option value="99">Журналистика</option>
                            <option value="134">Земельное право</option>
                            <option value="135">Земельные кадастры</option>
                            <option value="395">Зоология</option>
                            <option value="396">Зоотехния</option>
                            <option value="466">Иврит</option>
                            <option value="509">Издательское дело и полиграфия</option>
                            <option value="397">Иммунология</option>
                            <option value="548">Инвестиции</option>
                            <option value="549">Инвестиционный менеджмент</option>
                            <option value="82">Инженерная геодезия</option>
                            <option value="83">Инженерная геология</option>
                            <option value="296">Инженерная графика</option>
                            <option value="251">Инженерная психология</option>
                            <option value="550">Инновационный менеджмент</option>
                            <option value="510">Инструментальная промышленность</option>
                            <option value="551">Интернет маркетинг</option>
                            <option value="436">Информатика</option>
                            <option value="437">Информационная безопасность</option>
                            <option value="438">Информационные системы</option>
                            <option value="47">Информационные системы в экономике</option>
                            <option value="439">Информационные технологии</option>
                            <option value="440">Искусственный интеллект</option>
                            <option value="27">Искусство и культура</option>
                            <option value="28">Искусствоведение</option>
                            <option value="467">Испанский</option>
                            <option value="448">Исследование операций</option>
                            <option value="100">Историческая география</option>
                            <option value="230">История</option>
                            <option value="231">История архитектуры</option>
                            <option value="232">История балета</option>
                            <option value="136">История государства и права</option>
                            <option value="233">История Древняя</option>
                            <option value="234">История журналистики</option>
                            <option value="235">История Зарубежная</option>
                            <option value="236">История зарубежного искусства</option>
                            <option value="237">История искусств</option>
                            <option value="238">История культуры</option>
                            <option value="239">История Мировая</option>
                            <option value="240">История МЭО</option>
                            <option value="241">История Отечества</option>
                            <option value="186">История педагогики</option>
                            <option value="242">История развивающихся стран</option>
                            <option value="243">История развития политических учений и т</option>
                            <option value="244">История рекламы</option>
                            <option value="245">История религии</option>
                            <option value="246">История Средних веков</option>
                            <option value="48">История экономической мысли</option>
                            <option value="468">Итальянский</option>
                            <option value="469">Казахский</option>
                            <option value="167">Квантовая механика</option>
                            <option value="346">Квантовая физика</option>
                            <option value="329">Квантовая химия</option>
                            <option value="29">Кино и телевидение</option>
                            <option value="470">Китайский</option>
                            <option value="168">Классическая механика</option>
                            <option value="252">Клиническая психология</option>
                            <option value="253">Когнитивная психология</option>
                            <option value="330">Коллоидная химия</option>
                            <option value="552">Коммерция</option>
                            <option value="441">Компьютерная графика</option>
                            <option value="442">Компьютерные сети и системы</option>
                            <option value="137">Конституционное право</option>
                            <option value="254">Конфликтология</option>
                            <option value="138">Корпоративное право</option>
                            <option value="84">Краеведение</option>
                            <option value="139">Криминалистика</option>
                            <option value="140">Криминология</option>
                            <option value="443">Криптография</option>
                            <option value="30">Культурно-досуговая деятельность</option>
                            <option value="101">Культурология</option>
                            <option value="289">Ландшафтный дизайн</option>
                            <option value="471">Латинский</option>
                            <option value="511">Легкая промышленность</option>
                            <option value="102">Лексикология</option>
                            <option value="512">Лесное хозяйство</option>
                            <option value="13">Лесопромышленное машиностроение</option>
                            <option value="103">Лингвистика</option>
                            <option value="104">Лингвистика текста</option>
                            <option value="210">Линейная алгебра</option>
                            <option value="211">Линейное программирование</option>
                            <option value="105">Литература зарубежная</option>
                            <option value="106">Литература русская</option>
                            <option value="107">Литературоведение</option>
                            <option value="212">Логика</option>
                            <option value="108">Логика гуманитарная</option>
                            <option value="181">Логистика</option>
                            <option value="187">Логопедия</option>
                            <option value="49">Макроэкономика</option>
                            <option value="553">Маркетинг</option>
                            <option value="554">Маркетинг закупок</option>
                            <option value="555">Маркетинг продаж</option>
                            <option value="85">Маркшейдерское дело</option>
                            <option value="556">Масс-медиа</option>
                            <option value="449">Мат. мет. в экономике</option>
                            <option value="255">Мат. методы в психологии</option>
                            <option value="213">Математическая логика</option>
                            <option value="450">Математическая статистика</option>
                            <option value="347">Математическая физика</option>
                            <option value="331">Математическая химия</option>
                            <option value="214">Математические методы</option>
                            <option value="305">Математические методы в социологии</option>
                            <option value="50">Математические методы в экономике</option>
                            <option value="215">Математический анализ</option>
                            <option value="216">Математическое моделирование</option>
                            <option value="217">Математическое моделирование</option>
                            <option value="513">Материаловедение</option>
                            <option value="514">Материаловедение легкой промышленности</option>
                            <option value="451">Матпро</option>
                            <option value="398">Медицина</option>
                            <option value="256">Медицинская психология</option>
                            <option value="141">Международное право</option>
                            <option value="109">Международные отношения</option>
                            <option value="557">Международный маркетинг</option>
                            <option value="558">Международный менеджмент</option>
                            <option value="559">Менеджмент</option>
                            <option value="560">Менеджмент организации</option>
                            <option value="561">Менеджмент социально-культурной сферы</option>
                            <option value="515">Металлообработка</option>
                            <option value="516">Металлургия цветных металлов</option>
                            <option value="110">Метафизика (онтология)</option>
                            <option value="188">Методика преподавания</option>
                            <option value="189">Методика преподавания английского языка</option>
                            <option value="190">Методика преподавания техн. дисциплин</option>
                            <option value="191">Методика преподавания эконом. дисциплин</option>
                            <option value="348">Метрология</option>
                            <option value="290">Механизация строительства</option>
                            <option value="169">Механика</option>
                            <option value="170">Механика сплошных сред</option>
                            <option value="399">Микробиология</option>
                            <option value="86">Микроструктурная геология</option>
                            <option value="51">Микроэкономика</option>
                            <option value="349">Микроэлектроника</option>
                            <option value="31">Мировая культура моды</option>
                            <option value="32">Мировая Художественная Культура</option>
                            <option value="52">Мировая экономика</option>
                            <option value="517">Моделирование в металлургии</option>
                            <option value="489">МСФО</option>
                            <option value="33">Музыка</option>
                            <option value="142">Муниципальное право</option>
                            <option value="562">Муниципальное управление</option>
                            <option value="53">МЭО</option>
                            <option value="490">Налоги</option>
                            <option value="143">Налоговое право</option>
                            <option value="34">Народное искусство</option>
                            <option value="171">Наследственная механика</option>
                            <option value="297">Начертательная геометрия</option>
                            <option value="172">Небесная механика</option>
                            <option value="173">Неголономная механика</option>
                            <option value="257">Нейропсихология</option>
                            <option value="350">Нелинейная динамика</option>
                            <option value="472">Немецкий</option>
                            <option value="332">Неорганическая химия</option>
                            <option value="14">Нефтегазовое машиностроение</option>
                            <option value="518">Оборудование легкой промышленности</option>
                            <option value="519">Оборудование пищевой промышленности</option>
                            <option value="520">Обрабатывающее производство</option>
                            <option value="258">Общая психология</option>
                            <option value="111">Общественная мысль</option>
                            <option value="144">ОВД – органы внутренних дел</option>
                            <option value="444">ООП</option>
                            <option value="351">Оптика</option>
                            <option value="521">Организация общественного питания</option>
                            <option value="563">Организация производства и труда</option>
                            <option value="445">Организация ЭВМ</option>
                            <option value="333">Органическая химия</option>
                            <option value="400">Ортопедия</option>
                            <option value="446">Основы программирования</option>
                            <option value="522">Основы теории сварки и резки металлов</option>
                            <option value="352">ОТЦ</option>
                            <option value="87">Охрана среды</option>
                            <option value="523">Охрана труда и техника безопасности</option>
                            <option value="54">Оценка стоимости земли</option>
                            <option value="55">Оценка стоимости недвижимости</option>
                            <option value="401">Палеонтология</option>
                            <option value="524">Парикмахерское дело</option>
                            <option value="402">Патология</option>
                            <option value="259">Патопсихология</option>
                            <option value="291">ПГС</option>
                            <option value="192">Педагогика</option>
                            <option value="193">Педагогическая инноватика</option>
                            <option value="260">Педагогическая психология</option>
                            <option value="564">Планирование и прогнозирование</option>
                            <option value="15">Подъёмно-транспортные машины</option>
                            <option value="112">Политическая география</option>
                            <option value="35">Политическая культура/поведение</option>
                            <option value="113">Политические институты</option>
                            <option value="145">Политическое право</option>
                            <option value="114">Политология</option>
                            <option value="473">Польский</option>
                            <option value="474">Португальский</option>
                            <option value="146">Право</option>
                            <option value="147">Право интелектуальной собственности</option>
                            <option value="148">Право социального обеспечения</option>
                            <option value="149">Правовая статистика</option>
                            <option value="353">Практическая гидравлика</option>
                            <option value="565">Предпринимательство</option>
                            <option value="16">Приборостроение</option>
                            <option value="115">Прикладная лингвистика</option>
                            <option value="218">Прикладная математика</option>
                            <option value="174">Прикладная механика</option>
                            <option value="261">Прикладная психология</option>
                            <option value="292">Проектирование систем вентиляции, отопле</option>
                            <option value="293">Проектно-сметная документация</option>
                            <option value="525">Производственные технологии</option>
                            <option value="526">Производство металлических изделий и заг</option>
                            <option value="527">Производство технологического оборудован</option>
                            <option value="150">Прокурорский надзор</option>
                            <option value="528">Промышленное рыбоводство</option>
                            <option value="529">Промышленность бытовых приборов и машин</option>
                            <option value="116">Профессиональная этика</option>
                            <option value="262">Психоанализ</option>
                            <option value="263">Психодиагностика</option>
                            <option value="264">Психолингвистика</option>
                            <option value="265">Психология</option>
                            <option value="266">Психология восприятия</option>
                            <option value="267">Психология здоровья</option>
                            <option value="268">Психология личности</option>
                            <option value="269">Психология развития</option>
                            <option value="270">Психология спорта</option>
                            <option value="271">Психология труда</option>
                            <option value="272">Психопатология</option>
                            <option value="273">Психосоматика</option>
                            <option value="274">Психотерапия</option>
                            <option value="275">Психофизиология</option>
                            <option value="530">Радиотехническая и электронная промышлен</option>
                            <option value="354">Радиофизика</option>
                            <option value="355">Радиоэлектроника</option>
                            <option value="531">Ракетно-космическая отрасль</option>
                            <option value="566">Реклама и PR</option>
                            <option value="117">Религия и мифология</option>
                            <option value="175">Релятивистская механика</option>
                            <option value="567">Ресторанный бизнес</option>
                            <option value="151">Римское право</option>
                            <option value="118">Риторика</option>
                            <option value="356">Робототехника</option>
                            <option value="119">Русский язык</option>
                            <option value="491">Рынок ценных бумаг</option>
                            <option value="568">Связи с общественностью</option>
                            <option value="532">Сельское хозяйство</option>
                            <option value="17">Сельскохозяйственное машиностроение</option>
                            <option value="152">Семейное право</option>
                            <option value="403">Сестринское дело</option>
                            <option value="36">СКД (социально-культурная деятельность)</option>
                            <option value="182">Складская логистика</option>
                            <option value="176">Сопротивление материалов</option>
                            <option value="306">Социальная антропология</option>
                            <option value="194">Социальная педагогика</option>
                            <option value="276">Социальная психология</option>
                            <option value="307">Социальная работа</option>
                            <option value="308">Социальная статистика</option>
                            <option value="88">Социально – экономическая география</option>
                            <option value="37">Социально-культурное проектирование</option>
                            <option value="120">Социолингвистика</option>
                            <option value="309">Социология</option>
                            <option value="310">Социология знаний</option>
                            <option value="311">Социология коммуникаций</option>
                            <option value="312">Социология культуры</option>
                            <option value="313">Социология личности</option>
                            <option value="314">Социология молодежи</option>
                            <option value="315">Социология образования</option>
                            <option value="316">Социология организаций</option>
                            <option value="317">Социология политики</option>
                            <option value="318">Социология права</option>
                            <option value="319">Социология религии</option>
                            <option value="320">Социология риска</option>
                            <option value="321">Социология села</option>
                            <option value="322">Социология семьи</option>
                            <option value="323">Социология СМИ</option>
                            <option value="324">Социология труда</option>
                            <option value="195">Специальная педагогика (дефектология)</option>
                            <option value="302">Специальная подготовка</option>
                            <option value="277">Специальная психология</option>
                            <option value="196">Сравнительная педагогика</option>
                            <option value="278">Сравнительная психология</option>
                            <option value="533">Стандартизация и сертификация</option>
                            <option value="534">Станкостроение</option>
                            <option value="452">Статистика</option>
                            <option value="177">Статистическая механика</option>
                            <option value="357">Статистическая физика</option>
                            <option value="121">Стилистика</option>
                            <option value="569">Стратегический маркетинг</option>
                            <option value="570">Стратегический менеджмент</option>
                            <option value="56">Страхование</option>
                            <option value="453">Страховая математика</option>
                            <option value="178">Строительная механика</option>
                            <option value="18">Строительное и коммунальное машиностроен</option>
                            <option value="294">Строительство</option>
                            <option value="153">Судебная бухгалтерия</option>
                            <option value="154">Судебная экспертиза</option>
                            <option value="19">Судостроение</option>
                            <option value="197">Сурдопедагогика</option>
                            <option value="358">Схемотехника</option>
                            <option value="298">Схемы и чертежи в Visio</option>
                            <option value="57">Таможенная система</option>
                            <option value="155">Таможенное право</option>
                            <option value="535">ТАУ</option>
                            <option value="454">ТВиМС (теория вероятностей и мат. статис</option>
                            <option value="38">Театр</option>
                            <option value="359">Телекоммуникации</option>
                            <option value="179">Теоретическая механика</option>
                            <option value="360">Теоретические основы гидравлики</option>
                            <option value="219">Теория алгоритмов</option>
                            <option value="198">Теория воспитания</option>
                            <option value="156">Теория государства и права</option>
                            <option value="455">Теория графов</option>
                            <option value="220">Теория игр</option>
                            <option value="221">Теория информации</option>
                            <option value="361">Теория колебаний</option>
                            <option value="536">Теория металлургических процессов</option>
                            <option value="222">Теория множеств</option>
                            <option value="223">Теория оптимизации</option>
                            <option value="571">Теория организации</option>
                            <option value="362">Теория пластичности</option>
                            <option value="572">Теория принятия решений</option>
                            <option value="573">Теория принятия управленческих решений</option>
                            <option value="574">Теория управления</option>
                            <option value="363">Теория упругости</option>
                            <option value="364">Теория устойчивости и катастроф</option>
                            <option value="365">Теплотехника</option>
                            <option value="366">Термодинамика</option>
                            <option value="367">Техническая физика</option>
                            <option value="20">Техническое обслуживание и ремонт автомо</option>
                            <option value="21">Технология машиностроения</option>
                            <option value="537">Технология пищевой промышленности</option>
                            <option value="199">Тифлопедагогика</option>
                            <option value="22">ТММ</option>
                            <option value="58">Товароведение непродовольственных товаро</option>
                            <option value="59">Товароведение продовольственных товаров</option>
                            <option value="89">Топография</option>
                            <option value="224">Топология</option>
                            <option value="225">Топология</option>
                            <option value="368">ТОЭ</option>
                            <option value="23">Тракторостроение</option>
                            <option value="24">Транспорт</option>
                            <option value="183">Транспортная логистика</option>
                            <option value="157">Транспортное право</option>
                            <option value="295">Транспортное строительство</option>
                            <option value="158">Трудовое право</option>
                            <option value="575">Туризм</option>
                            <option value="226">ТФКП</option>
                            <option value="369">ТЭЦ</option>
                            <option value="159">Уголовное право</option>
                            <option value="475">Украинский</option>
                            <option value="576">Управление качеством</option>
                            <option value="577">Управление персоналом</option>
                            <option value="578">Управление проектами</option>
                            <option value="579">Управление рисками</option>
                            <option value="492">Учет в АПК</option>
                            <option value="493">Учет в банковской сфере</option>
                            <option value="494">Учет в промышленности</option>
                            <option value="495">Учет в строительстве</option>
                            <option value="496">Учет в сфере обслуживания</option>
                            <option value="497">Учет в торговле</option>
                            <option value="334">Фармацевтическая химия</option>
                            <option value="370">Физика</option>
                            <option value="371">Физика атмосферы</option>
                            <option value="372">Физика высоких энергий</option>
                            <option value="373">Физика конденсированных сред</option>
                            <option value="374">Физика плазмы</option>
                            <option value="375">Физика твердого тела/атомов/молекул/нано</option>
                            <option value="376">Физика элементарных частиц</option>
                            <option value="404">Физиология</option>
                            <option value="90">Физическая география</option>
                            <option value="335">Физическая химия</option>
                            <option value="377">Физические основы электроники</option>
                            <option value="200">Физкультура и спорт</option>
                            <option value="122">Филология</option>
                            <option value="123">Философия</option>
                            <option value="160">Философия права</option>
                            <option value="498">Финансовая математика</option>
                            <option value="161">Финансовое право</option>
                            <option value="580">Финансовый менеджмент</option>
                            <option value="499">Финансы</option>
                            <option value="476">Финский</option>
                            <option value="39">Фольклор</option>
                            <option value="40">Фотография</option>
                            <option value="477">Французский</option>
                            <option value="227">Функциональный анализ</option>
                            <option value="336">Химическая термодинамика</option>
                            <option value="378">Химическая физика</option>
                            <option value="25">Химическое машиностроение</option>
                            <option value="337">Химия</option>
                            <option value="405">Хирургия</option>
                            <option value="162">Хозяйственное право</option>
                            <option value="538">Холодильное оборудование</option>
                            <option value="41">Хореография</option>
                            <option value="500">Ценообразование</option>
                            <option value="406">Цитология</option>
                            <option value="379">Цифровая обработка сигнала</option>
                            <option value="299">Чертежи в AutoCad</option>
                            <option value="300">Чертежи в SolidWorks</option>
                            <option value="301">Чертежи в КОМПАС</option>
                            <option value="478">Чешский</option>
                            <option value="456">Численные методы</option>
                            <option value="479">Шведский</option>
                            <option value="201">Школоведение</option>
                            <option value="228">Школьная математика</option>
                            <option value="163">Экологическое право</option>
                            <option value="91">Экология</option>
                            <option value="457">Эконометрика</option>
                            <option value="60">Экономика</option>
                            <option value="61">Экономика АПК</option>
                            <option value="62">Экономика в строительстве</option>
                            <option value="63">Экономика отрасли</option>
                            <option value="64">Экономика предприятия</option>
                            <option value="65">Экономика природопользования</option>
                            <option value="66">Экономика формации</option>
                            <option value="92">Экономическая география</option>
                            <option value="279">Экономическая психология</option>
                            <option value="67">Экономическая социология</option>
                            <option value="458">Экономическая статистика</option>
                            <option value="68">Экономическая теория</option>
                            <option value="164">Экономическое право</option>
                            <option value="325">Экосоциология</option>
                            <option value="42">Экскурсоведение</option>
                            <option value="280">Экспериментальная психология</option>
                            <option value="380">Электродинамика</option>
                            <option value="180">Электромеханика</option>
                            <option value="381">Электроника</option>
                            <option value="382">Электросвязь</option>
                            <option value="383">Электроснабжение</option>
                            <option value="384">Электротехника</option>
                            <option value="539">Электротехническая промышленность</option>
                            <option value="540">Электроэнергетика</option>
                            <option value="385">Эмиссионная электроника</option>
                            <option value="459">ЭММ</option>
                            <option value="541">Энергетика</option>
                            <option value="542">Энергосбережение</option>
                            <option value="124">Эпистемология</option>
                            <option value="581">Эргономика</option>
                            <option value="125">Эстетика</option>
                            <option value="126">Этика</option>
                            <option value="127">Этнолингвистика</option>
                            <option value="326">Этносоциология</option>
                            <option value="407">Этология</option>
                            <option value="281">Юридическая психология</option>
                            <option value="165">Юриспруденция</option>
                            <option value="386">Ядерная физика</option>
                            <option value="128">Языкознание</option>
                            <option value="480">Японский</option>
                        </select>
                    </div>

                    <div class="order-form__group">
                        <label class="order-form__label">Ваша тема</label>
                        <input class="order-form__input" type="text" name="Topic" required
                               placeholder="Тема работы или заголовок">
                    </div>


                    <div class="row">


                        <div class="col-md-6">
                            <div class="order-form__group">
                                <label class="order-form__label">Срок сдачи</label>
                                <input class="order-form__input orderDeadline" type="text" name="srok" required
                                       placeholder="Срок">
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="order-form__group">
                                <label class="order-form__label">Количество страниц</label>
                                <select class="order-form__input" name="pages">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                    <option value="32">32</option>
                                    <option value="33">33</option>
                                    <option value="34">34</option>
                                    <option value="35">35</option>
                                    <option value="36">36</option>
                                    <option value="37">37</option>
                                    <option value="38">38</option>
                                    <option value="39">39</option>
                                    <option value="40">40</option>
                                    <option value="41">41</option>
                                    <option value="42">42</option>
                                    <option value="43">43</option>
                                    <option value="44">44</option>
                                    <option value="45">45</option>
                                    <option value="46">46</option>
                                    <option value="47">47</option>
                                    <option value="48">48</option>
                                    <option value="49">49</option>
                                    <option value="50">50</option>
                                    <option value="51">51</option>
                                    <option value="52">52</option>
                                    <option value="53">53</option>
                                    <option value="54">54</option>
                                    <option value="55">55</option>
                                    <option value="56">56</option>
                                    <option value="57">57</option>
                                    <option value="58">58</option>
                                    <option value="59">59</option>
                                    <option value="60">60</option>
                                    <option value="61">61</option>
                                    <option value="62">62</option>
                                    <option value="63">63</option>
                                    <option value="64">64</option>
                                    <option value="65">65</option>
                                    <option value="66">66</option>
                                    <option value="67">67</option>
                                    <option value="68">68</option>
                                    <option value="69">69</option>
                                    <option value="70">70</option>
                                    <option value="71">71</option>
                                    <option value="72">72</option>
                                    <option value="73">73</option>
                                    <option value="74">74</option>
                                    <option value="75">75</option>
                                    <option value="76">76</option>
                                    <option value="77">77</option>
                                    <option value="78">78</option>
                                    <option value="79">79</option>
                                    <option value="80">80</option>
                                    <option value="81">81</option>
                                    <option value="82">82</option>
                                    <option value="83">83</option>
                                    <option value="84">84</option>
                                    <option value="85">85</option>
                                    <option value="86">86</option>
                                    <option value="87">87</option>
                                    <option value="88">88</option>
                                    <option value="89">89</option>
                                    <option value="90">90</option>
                                    <option value="91">91</option>
                                    <option value="92">92</option>
                                    <option value="93">93</option>
                                    <option value="94">94</option>
                                    <option value="95">95</option>
                                    <option value="96">96</option>
                                    <option value="97">97</option>
                                    <option value="98">98</option>
                                    <option value="99">99</option>
                                    <option value="100">100</option>
                                    <option value="101">101</option>
                                    <option value="102">102</option>
                                    <option value="103">103</option>
                                    <option value="104">104</option>
                                    <option value="105">105</option>
                                    <option value="106">106</option>
                                    <option value="107">107</option>
                                    <option value="108">108</option>
                                    <option value="109">109</option>
                                    <option value="110">110</option>
                                    <option value="111">111</option>
                                    <option value="112">112</option>
                                    <option value="113">113</option>
                                    <option value="114">114</option>
                                    <option value="115">115</option>
                                    <option value="116">116</option>
                                    <option value="117">117</option>
                                    <option value="118">118</option>
                                    <option value="119">119</option>
                                    <option value="120">120</option>
                                    <option value="121">121</option>
                                    <option value="122">122</option>
                                    <option value="123">123</option>
                                    <option value="124">124</option>
                                    <option value="125">125</option>
                                    <option value="126">126</option>
                                    <option value="127">127</option>
                                    <option value="128">128</option>
                                    <option value="129">129</option>
                                    <option value="130">130</option>
                                    <option value="131">131</option>
                                    <option value="132">132</option>
                                    <option value="133">133</option>
                                    <option value="134">134</option>
                                    <option value="135">135</option>
                                    <option value="136">136</option>
                                    <option value="137">137</option>
                                    <option value="138">138</option>
                                    <option value="139">139</option>
                                    <option value="140">140</option>
                                    <option value="141">141</option>
                                    <option value="142">142</option>
                                    <option value="143">143</option>
                                    <option value="144">144</option>
                                    <option value="145">145</option>
                                    <option value="146">146</option>
                                    <option value="147">147</option>
                                    <option value="148">148</option>
                                    <option value="149">149</option>
                                    <option value="150">150</option>
                                </select>
                            </div>
                        </div>


                    </div>


                    <div class="order-form__group">
                        <textarea class="order-form__input" name="must_do" rows="5"
                                  placeholder="Комментарий (Не обязательно)"></textarea>
                    </div>


                    <h3 class="order-form__title">
                        <span>Уточните задание</span>
                    </h3>
                    <div class="order-form__group">
                        <label class="order-form__label">Эл. почта</label>
                        <input class="order-form__input" type="email" name="Email" required
                               placeholder="mail@mail.com">
                    </div>


                    <div class="order-form__group">
                        <label class="order-form__label">Телефон</label>
                        <input class="order-form__input phone-mask" type="text" name="Phone"
                               placeholder="+7 (___) ___-__-__">
                    </div>


                    <input name="ref_id" type="hidden" value="<?php the_field( 'user_id_ref', 'option' ); ?>">
                    <input name="Country" type="hidden" value="2">
                    <div class="order-form__group order-form__group--button">
                        <button class="button button--blue" type="submit">Отправить заявку на оценку</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- DATETIME LIBRARY -->
<link rel="stylesheet" href="https://edu-masters.com/form/jquery.datetimepicker.min.css">
<script src="https://edu-masters.com/form/jquery.datetimepicker.full.min.js"></script>
<!-- MASK -->
<script src="https://edu-masters.com/form/jquery.maskedinput.min.js"></script>

<script>
    if (typeof jQuery === "undefined") {
        var scriptElement = document.createElement("script");
        scriptElement.setAttribute("src", "https://edu-masters.com/form/jquery.min.2.js");
        scriptElement.setAttribute("crossorigin", "anonymous");
        scriptElement.onload = function () {
            init();
        };
        document.head.insertAdjacentElement("beforeend", scriptElement);

    }

    document.addEventListener("DOMContentLoaded", function () {
        $('.na5ku-calcForm2 .orderDeadline').datetimepicker({
            timepicker: true,
            format: 'Y/m/d H:i' //another format will be ignored
        });


        //$('.select-input-select2').select2();

        $('.na5ku-calcForm2 .phone-mask').mask('+7 (999) 999-99-99');
    }, false);

</script>

