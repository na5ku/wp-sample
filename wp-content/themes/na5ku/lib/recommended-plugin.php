<?php

require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'na5ku_register_required_plugins' );

function na5ku_register_required_plugins() {

	$plugins = array(
		
		//ACF Pro
		array(
			'name'               => esc_html__('Advance Custom Field Pro', 'na5ku'),
			'slug'               => 'advanced-custom-fields-pro',
			'source'             => get_stylesheet_directory() . '/lib/plugins/advanced-custom-fields-pro.zip', 
			'required'           => true,
			'force_activation'   => false,
			'force_deactivation' => false,
		),

		//WPML
		array(
			'name'               => esc_html__('WPML - Мультиязычность', 'na5ku'),
			'slug'               => 'sitepress-multilingual-cms',
			'source'             => get_stylesheet_directory() . '/lib/plugins/sitepress-multilingual-cms.zip', 
			'required'           => false,
		),

		//WPML String Translation
		array(
			'name'               => esc_html__('WPML String translation', 'na5ku'),
			'slug'               => 'wpml-string-translation',
			'source'             => get_stylesheet_directory() . '/lib/plugins/wpml-string-translation.zip', 
			'required'           => false,
		),

		//WPML YOAST SEO
		/*array(
			'name'               => esc_html__('WPML Yoast seo', 'na5ku'),
			'slug'               => 'wp-seo-multilingual',
			'source'             => get_stylesheet_directory() . '/lib/plugins/wp-seo-multilingual.zip', 
			'required'           => false,
		),*/

		// Na5ku Plugin
		array(
			'name'      => esc_html__('Cyr-To-Lat', 'na5ku'),
			'slug'      => 'cyr2lat',
			'required'  => false,
		),

		array(
			'name'      => esc_html__('All-in-One WP Migration', 'na5ku'),
			'slug'      => 'all-in-one-wp-migration',
			'required'  => false,
		),
	);

	$config = array(
		'id'           => 'tgmpa',
		'default_path' => '',
		'menu'         => 'tgmpa-install-plugins',
		'parent_slug'  => 'themes.php',
		'capability'   => 'edit_theme_options',
		'has_notices'  => true, 
		'dismissable'  => true, 
		'dismiss_msg'  => '',   
		'is_automatic' => false, 
		'message'      => '',                      
	);
	tgmpa( $plugins, $config );
}