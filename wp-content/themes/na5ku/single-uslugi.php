
<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<section class="indiv_first_section_wrap">
		<div class="container">
			<div class="indiv_first_section">
				<ul class="indiv_first_breadcrumbs">
					<li>
						<a href="/">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/bread_home.svg" alt="<?php the_title(); ?>">
							<span><?php echo esc_html__('Главная','na5ku'); ?></span>
						</a>
					</li>
					<!--<li>
						<a href="#">Услуги</a>
					</li>-->
					<li><?php the_title(); ?></li>
				</ul>

				<div class="main_first_title indiv_first_title">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-3.svg" alt="<?php the_field( 'serv_first_title' ); ?>">
					<span><?php the_field( 'serv_first_title' ); ?></span>
				</div>
				<?php if ( get_field( 'serv_short_descr') ) : ?>
					<div class="indiv_first_desc"><?php the_field( 'serv_short_descr' ); ?></div>
				<?php endif; ?>

				<?php if( get_field('serv_first_adv_on') == 1){ ?>
					<?php if ( have_rows( 'serv_adv_mains', 'option' ) ) { ?>
						<div class="indiv_first_list">
							<?php while ( have_rows( 'serv_adv_mains', 'option' ) ) : the_row(); ?>
								<div class="indiv_first_list_item">
									<img src="<?php the_sub_field( 'serv_adv_icon_main' ); ?>" />
									<div class="indiv_first_list_item_text"><?php the_sub_field( 'serv_adv_text_main' ); ?></div>
								</div>
							<?php endwhile; ?>
						</div>
					<?php } else { ?>
						<div class="indiv_first_list">
							<div class="indiv_first_list_item">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/main_first_item_1.svg">
								<div class="indiv_first_list_item_text"><p><strong><?php echo esc_html__('Презентация, доклад и отчет','na5ku'); ?></strong><br><?php echo esc_html__('о уникальности — в подарок!','na5ku'); ?></p>
							</div>
						</div>
						<div class="indiv_first_list_item">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/indiv_first_item_2.svg">
							<div class="indiv_first_list_item_text"><p><strong><?php echo esc_html__('Проверяем работы','na5ku'); ?></strong><?php echo esc_html__(' на соответствие ГОСТам, требованиям ВУЗа и нормам Антиплагиата','na5ku'); ?> </p>
							</div>
						</div>
						<div class="indiv_first_list_item">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/indiv_first_item_3.svg">
							<div class="indiv_first_list_item_text"><p><strong><?php echo esc_html__('Полная оплата','na5ku'); ?></strong><?php echo esc_html__(' — после ознакомления с работой.','na5ku'); ?><br>
							<?php echo esc_html__('Гарантия возврата средств.','na5ku'); ?></p>
						</div>
					</div>
				</div>
			<?php } ?>
		<?php } else { ?>
			<?php if ( have_rows( 'serv_adv' ) ) : ?>
				<div class="indiv_first_list">
					<?php while ( have_rows( 'serv_adv' ) ) : the_row(); ?>
						<div class="indiv_first_list_item">
							<img src="<?php the_sub_field( 'serv_adv_icon' ); ?>" />
							<div class="indiv_first_list_item_text"><?php the_sub_field( 'serv_adv_text' ); ?></div>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		<?php } ?>

		<?php if ( get_field( 'serv_first_btn_title') ) : ?>
			<div class="indiv_first_price_btn blue_btn"><a href="<?php the_field( 'serv_first_btn_href' ); ?>"><?php the_field( 'serv_first_btn_title' ); ?></a></div>
		<?php endif; ?>
	</div>
</div>
</section>
<section class="calculate_price_section_wrap" id="order_form">
	<div class="container">
		<div class="calculate_price_section indiv">
			<div class="calculate_price_left">
				<div class="calculate_price_title"><?php the_field( 'serv_form_order_title' ); ?></div>
				<div class="calculate_price_desc"><?php the_field( 'serv_form_order_descr' ); ?></div>
                    <?php if ( get_field( 'short_code_form_order', 'option' ) ) { ?>
                    	<?php echo do_shortcode( get_field( 'short_code_form_order', 'option' ) ); ?>
                    <?php } else { ?>
                    	<?php 
                    	$form_lang = ( get_field( 'user_country', 'option' ) == 1 ) ? 'form_order_uk.php' : 'form_order.php';
                    	include_once get_template_directory() . '/inc/form/'.$form_lang; ?>
                    <?php } ?>
			</div>
			<div class="calculate_price_right">
				<?php if ( have_rows( 'serv_form_order_adv_list' ) ) { ?>
					<?php while ( have_rows( 'serv_form_order_adv_list' ) ) : the_row(); ?>
						<div class="calculate_price_right_item">
							<div class="calculate_price_right_item_img">
								<img src="<?php the_sub_field( 'serv_form_order_adv_icon' ); ?>" alt="<?php the_sub_field( 'serv_form_order_adv_title' ); ?>">
							</div>
							<div class="calculate_price_right_item_content">
								<div class="calculate_price_right_item_title"><?php the_sub_field( 'serv_form_order_adv_title' ); ?></div>
								<div class="calculate_price_right_item_text"><?php the_sub_field( 'serv_form_order_adv_descr' ); ?></div>
							</div>
						</div>

					<?php endwhile; ?>

				<?php } else { ?>
								<div class="calculate_price_right_item">
							<div class="calculate_price_right_item_img">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/calc_1.svg" alt="Объем:">
							</div>
							<div class="calculate_price_right_item_content">
								<div class="calculate_price_right_item_title"><?php echo esc_html__('Объем:','na5ku'); ?></div>
								<div class="calculate_price_right_item_text"><?php echo esc_html__('От 80 страниц','na5ku'); ?></div>
							</div>
						</div>

											<div class="calculate_price_right_item">
							<div class="calculate_price_right_item_img">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/calc_2.svg" alt="Рекомендуемый срок:">
							</div>
							<div class="calculate_price_right_item_content">
								<div class="calculate_price_right_item_title"><?php echo esc_html__('Рекомендуемый срок:','na5ku'); ?></div>
								<div class="calculate_price_right_item_text"><?php echo esc_html__('От 10 дней','na5ku'); ?></div>
							</div>
						</div>

											<div class="calculate_price_right_item">
							<div class="calculate_price_right_item_img">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/calc_3.svg" alt="Срочный заказ:">
							</div>
							<div class="calculate_price_right_item_content">
								<div class="calculate_price_right_item_title"><?php echo esc_html__('Срочный заказ:','na5ku'); ?></div>
								<div class="calculate_price_right_item_text"><?php echo esc_html__('от 3 дней','na5ku'); ?></div>
							</div>
						</div>

											<div class="calculate_price_right_item">
							<div class="calculate_price_right_item_img">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/calc_4.svg" alt="Автор:">
							</div>
							<div class="calculate_price_right_item_content">
								<div class="calculate_price_right_item_title"><?php echo esc_html__('Автор:','na5ku'); ?></div>
								<div class="calculate_price_right_item_text"><?php echo esc_html__('Преподаватель или кандидат наук','na5ku'); ?></div>
							</div>
						</div>

											<div class="calculate_price_right_item">
							<div class="calculate_price_right_item_img">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/calc_5.svg" alt="Бесплатные правки:">
							</div>
							<div class="calculate_price_right_item_content">
								<div class="calculate_price_right_item_title"><?php echo esc_html__('Бесплатные правки:','na5ku'); ?></div>
								<div class="calculate_price_right_item_text"><?php echo esc_html__('1 месяц','na5ku'); ?></div>
							</div>
						</div>

											<div class="calculate_price_right_item">
							<div class="calculate_price_right_item_img">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/calc_6.svg" alt="Бесплатные материалы:">
							</div>
							<div class="calculate_price_right_item_content">
								<div class="calculate_price_right_item_title"><?php echo esc_html__('Бесплатные материалы:','na5ku'); ?></div>
								<div class="calculate_price_right_item_text"><?php echo esc_html__('План, презентация, речь, отчет о уникальности','na5ku'); ?></div>
							</div>
						</div>

											<div class="calculate_price_right_item">
							<div class="calculate_price_right_item_img">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/calc_7.svg" alt="Оригинальность:">
							</div>
							<div class="calculate_price_right_item_content">
								<div class="calculate_price_right_item_title"><?php echo esc_html__('Оригинальность:','na5ku'); ?></div>
								<div class="calculate_price_right_item_text"><?php echo esc_html__('от 60% (достаточно для большинства ВУЗов)','na5ku'); ?></div>
							</div>
						</div>

				<?php } ?>
			</div>
		</div>
	</div>
</section>
<?php if ( get_field( 'home_title_3', 'option' ) ) : ?>
	<section class="how_it_works_section_wrap ">
		<div class="container">
			<div class="how_it_works_section">
				<div class="how_it_works_title">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="">
					<span><?php the_field( 'home_title_3', 'option' ); ?></span>
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-left-4.svg" alt="">
				</div>
				<div class="how_it_works_tabs">
					<div class="how_it_works_steps">
						<?php if ( have_rows( 'home_slide_3', 'option' ) ) : ?>
							<?php $total_rows = count(get_field('home_slide_3', 'option')); while ( have_rows( 'home_slide_3', 'option' ) ) : the_row(); ?>
							<?php $active = (get_row_index() == 1) ? 'active' : ''; ?>
							<div class="how_it_works_step_number <?php echo $active; ?>"><?php echo get_row_index(); ?></div>
							<?php if($total_rows != get_row_index()){ ?>
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/how_it_works_dots_arr.svg" alt="">
							<?php } ?>

						<?php endwhile; ?>

					<?php endif; ?>

				</div>

				<div class="how_it_works_links">
					<?php if ( have_rows( 'home_slide_3', 'option' ) ) : ?>
						<?php while ( have_rows( 'home_slide_3', 'option' ) ) : the_row(); ?>
							<?php $active = (get_row_index() == 1) ? 'active' : ''; ?>
							<div class="how_it_works_link_item <?php echo $active; ?>"><?php the_sub_field( 'home_slide_title_3' ); ?> </div>

						<?php endwhile; ?>

					<?php endif; ?>
				</div>
				<div class="how_it_works_tab_content">
					<?php if ( have_rows( 'home_slide_3', 'option' ) ) : ?>
						<?php while ( have_rows( 'home_slide_3', 'option' ) ) : the_row(); ?>
							<?php $active = (get_row_index() == 1) ? 'active' : ''; ?>
							<img class="how_it_works_tab_item <?php echo $active; ?>" src="<?php the_sub_field( 'home_slide_img_3' ); ?>" alt="<?php the_sub_field( 'home_slide_title_3' ); ?>">
						<?php endwhile; ?>

					<?php endif; ?>
				</div>

			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<?php if(get_field( 'serv_faq_option' ) == 1){ ?>
	<?php include_once get_template_directory() . '/inc/faq_indiv.php'; ?>
<?php } else { ?>
	<?php include_once get_template_directory() . '/inc/faq_main.php'; ?>
<?php } ?>

<?php include_once get_template_directory() . '/inc/section_example_work.php'; ?>

<?php if ( get_field( 'opt_serv_adv', 'option' ) ) : ?>
	<section class="indiv_features_section_wrap">
		<div class="container">
			<div class="indiv_features_list">
				<?php while ( have_rows( 'opt_serv_adv', 'option' ) ) : the_row(); ?>

					<div class="indiv_features_item">
						<img src="<?php the_sub_field( 'opt_serv_adv_icon' ); ?>" alt="<?php the_sub_field( 'opt_serv_adv_title' ); ?>" class="indiv_features_item_icon">
						<div class="indiv_features_item_right">
							<div class="indiv_features_item_title"><?php the_sub_field( 'opt_serv_adv_title' ); ?></div>
							<div class="indiv_features_item_text"><?php the_sub_field( 'opt_serv_adv_descr' ); ?></div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php if ( get_field( 'opt_serv_part', 'option' ) ) : ?>
	<section class="indiv_we_help_section_wrap">
		<div class="container">
			<div class="indiv_we_help_section">
				<div class="how_it_works_title we_help">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="">
					<span><?php the_field( 'opt_serv_part_sec_title', 'option' ); ?></span>
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-left-4.svg" alt="">
				</div>
				<div class="indiv_we_help_slider" id="indiv_we_help_slider">
					<?php while ( have_rows( 'opt_serv_part', 'option' ) ) : the_row(); ?>
						<div class="indiv_we_help_slider_item_wrap">
							<div class="indiv_we_help_slider_item">
								<img src="<?php the_sub_field( 'opt_serv_part_icon' ); ?>" alt="<?php the_sub_field( 'opt_serv_part_title' ); ?>" class="indiv_we_help_slider_item_img">
								<a href="<?php the_sub_field( 'opt_serv_part_href' ); ?>" target="_blank"><div class="indiv_we_help_slider_item_text"><?php the_sub_field( 'opt_serv_part_title' ); ?></div></a>
							</div>
						</div>
					<?php endwhile; ?>


				</div>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php if ( get_field( 'serv_page_seo_title_1') ) : ?>
	<section class="how_to_order_section_wrap text_section">
		<div class="container">
			<div class="how_to_order_section">
				<div class="seo_text">
					<div class="how_to_order_title">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="<?php the_field( 'serv_page_seo_title_1'); ?>">
						<span><?php the_field( 'serv_page_seo_title_1'); ?></span>
					</div>
					<?php the_field( 'serv_page_seo_text_1'); ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php if ( have_rows( 'serv_page_steps' ) && get_field( 'steps_option_on') == 1 ) : ?>
<section class="indiv_stages_section_wrap">
	<div class="container">
		<div class="indiv_stages_section">
			<div class="how_it_works_title">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-right-4.svg" alt="<?php the_field( 'serv_page_steps_main_title' ); ?>">
				<span><?php the_field( 'serv_page_steps_main_title' ); ?></span>
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/play-left-4.svg" alt="<?php the_field( 'serv_page_steps_main_title' ); ?>">
			</div>
			<div class="indiv_stages_grid">
				<?php $num = 0; while ( have_rows( 'serv_page_steps' ) ) : the_row(); $num++; ?>
				<div class="indiv_stages_item">
					<div class="indiv_stages_item_title"><?php the_sub_field( 'serv_page_steps_title' ); ?> <span><?php echo $num; ?></span></div>
					<div class="indiv_stages_item_text"><?php the_sub_field( 'serv_page_steps_descr' ); ?></div>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</div>
</section>
<?php endif; ?>
<br>
<?php include_once get_template_directory() . '/inc/service_style.php'; ?>
<?php endwhile; ?>

<?php get_footer(); ?>
