jQuery(document).ready(function () {
    let NA5KU_API_URL_LOCAL = 'https://api.edu-masters.com/';
    let NA5KU_VALUTA_LOCAL = 1;

    if(typeof (NA5KU_API_URL) !== 'undefined'){
        NA5KU_API_URL_LOCAL = NA5KU_API_URL;
    }

    if(typeof (NA5KU_VALUTA_THEME) !== 'undefined'){
        NA5KU_VALUTA_LOCAL = NA5KU_VALUTA_THEME;
    }

    if(typeof (NA5KU_VALUTA) !== 'undefined'){
        NA5KU_VALUTA_LOCAL = NA5KU_VALUTA;
    }



    $.ajax({
        url: NA5KU_API_URL_LOCAL + "ordersGuest/getFinishedList",
        method: 'POST',
        data: {
            "data": {
                "valuta": NA5KU_VALUTA_LOCAL
            }
        },
        dataType: 'JSON'
    }).done(function (json) {
        if (json && json.code == 200 && json.data) {
            var order, desktopHTML = '', mobileHTML = '';
            for (var i = 0; i < json.data.length; i++) {
                order = json.data[i];
                desktopHTML += '<div class="examples_last_works_row">';
                desktopHTML += '<div class="examples_last_works_col_1">' + order.date + '</div>';
                desktopHTML += '<div class="examples_last_works_col_2">' + order.id + '</div>';
                desktopHTML += '<div class="examples_last_works_col_3">' + order.topic + '</div>';
                desktopHTML += '<div class="examples_last_works_col_4">' + order.type + '</div>';
                desktopHTML += '</div>';

            }
            $('.examples_last_works_head').after(desktopHTML);
        } else {
            console.log('GET LIST FAILED - EMPTY!!!');
        }
    }).fail(function () {
        console.log('GET LIST FAILED!!!');
    });
});

