$(document).ready(function () {
    var timer = null;

    function slide_interval(time){
        var slides1 = $('.how_it_works_link_item'), slides2 = $('.how_it_works_step_number'), slides3 = $('.how_it_works_tab_item'),
        counter = 0;

        if (timer !== null) return;

        timer = setInterval(function(){
            slides1.removeClass('active');
            slides1.eq(counter).addClass('active');
            slides2.removeClass('active');
            slides2.eq(counter).addClass('active');
            slides3.removeClass('active');
            slides3.eq(counter).addClass('active');
            counter++;
            if (counter == slides1.length) counter = 0;
        }, time);
    }

    slide_interval(4000);

    $(".how_it_works_tabs .how_it_works_link_item").click(function () {
        clearInterval(timer);
        timer = null;
    });

    //блок Узнайте точную стоимость

    $(document).click(function (el) {
        //hide calculate select list
        if (!el.target.closest(".calculate_price_select")){
            $(".calculate_price_select").removeClass("open");
        }
    });

    $(".calculate_price_select").click(function () {
        if ($(this).hasClass("open")){
            $(this).removeClass("open")
        } else {
            $(".calculate_price_select").removeClass("open")
            $(this).addClass("open")
        }

    });

    $(".calculate_price_select_list_item input").change(function () {
        let thisValue = $(this).val();

        $(this).parents(".calculate_price_select").find(".calculate_price_select_head span").text(thisValue);
        $(this).parents(".calculate_price_select").removeClass("open");
        $(this).parents(".calculate_price_select").addClass("selected")
    });

    // табы Примеры выполненных работ и т.д.

    $("#examples_list_slider").slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: true,
        dotsClass: 'examples_list_slider_dots',
        responsive: [
            {
                breakpoint: 1179,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1
                }
            },
        ],
        prevArrow: '<button class="examples_list_slider_btn prev_btn">\n' +
            '                            <svg width="28" height="14" viewBox="0 0 28 14">\n' +
            '                                <path d="M0.320796 7.77422L0.32178 7.77526L6.03684 13.4628C6.46499 13.8888 7.1575 13.8872 7.58368 13.459C8.0098 13.0309 8.00816 12.3384 7.58001 11.9123L3.74292 8.09376L26.9063 8.09376C27.5103 8.09376 28 7.60409 28 7.00001C28 6.39594 27.5103 5.90626 26.9063 5.90626L3.74298 5.90626L7.57996 2.08777C8.00811 1.66164 8.00975 0.969136 7.58363 0.540988C7.15745 0.112731 6.46488 0.111255 6.03679 0.53727L0.321725 6.22476L0.32074 6.2258C-0.107627 6.65335 -0.10626 7.3481 0.320796 7.77422Z"/>\n' +
            '                            </svg>\n' +
            '                        </button>',
        nextArrow: '<button class="examples_list_slider_btn next_btn">\n' +
            '                            <svg width="28" height="14" viewBox="0 0 28 14">\n' +
            '                                <path d="M27.6792 6.22578C27.6789 6.22545 27.6786 6.22507 27.6782 6.22474L21.9632 0.53725C21.535 0.11118 20.8425 0.112766 20.4163 0.540968C19.9902 0.969116 19.9918 1.66162 20.42 2.08775L24.2571 5.90624L1.09375 5.90624C0.489671 5.90624 -1.08005e-06 6.39591 -1.18567e-06 6.99999C-1.29129e-06 7.60406 0.489671 8.09373 1.09375 8.09373L24.257 8.09374L20.42 11.9122C19.9919 12.3384 19.9903 13.0309 20.4164 13.459C20.8426 13.8873 21.5351 13.8887 21.9632 13.4627L27.6783 7.77524C27.6786 7.77491 27.6789 7.77453 27.6793 7.7742C28.1076 7.34666 28.1063 6.65191 27.6792 6.22578Z"/>\n' +
            '                            </svg>\n' +
            '                        </button>',

    })

    $(".examples_tabs_nav_link").click(function (e){
        e.preventDefault()
        let thisHref = $(this).attr("href");
        let thisIndex = $(this).index();

        $(".examples_tabs_nav_link").removeClass("active");
        $(this).addClass("active")

        $(".examples_tabs_content").removeClass("active");
        $(thisHref).addClass("active")

        if (thisIndex === 2){
            $("#examples_tab_reviews_slider").slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                dots: true,
                dotsClass: 'examples_list_slider_dots',
                responsive: [
                    {
                        breakpoint: 1179,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 1
                        }
                    },
                ],
                prevArrow: '<button class="examples_list_slider_btn prev_btn">\n' +
                    '                            <svg width="28" height="14" viewBox="0 0 28 14">\n' +
                    '                                <path d="M0.320796 7.77422L0.32178 7.77526L6.03684 13.4628C6.46499 13.8888 7.1575 13.8872 7.58368 13.459C8.0098 13.0309 8.00816 12.3384 7.58001 11.9123L3.74292 8.09376L26.9063 8.09376C27.5103 8.09376 28 7.60409 28 7.00001C28 6.39594 27.5103 5.90626 26.9063 5.90626L3.74298 5.90626L7.57996 2.08777C8.00811 1.66164 8.00975 0.969136 7.58363 0.540988C7.15745 0.112731 6.46488 0.111255 6.03679 0.53727L0.321725 6.22476L0.32074 6.2258C-0.107627 6.65335 -0.10626 7.3481 0.320796 7.77422Z"/>\n' +
                    '                            </svg>\n' +
                    '                        </button>',
                nextArrow: '<button class="examples_list_slider_btn next_btn">\n' +
                    '                            <svg width="28" height="14" viewBox="0 0 28 14">\n' +
                    '                                <path d="M27.6792 6.22578C27.6789 6.22545 27.6786 6.22507 27.6782 6.22474L21.9632 0.53725C21.535 0.11118 20.8425 0.112766 20.4163 0.540968C19.9902 0.969116 19.9918 1.66162 20.42 2.08775L24.2571 5.90624L1.09375 5.90624C0.489671 5.90624 -1.08005e-06 6.39591 -1.18567e-06 6.99999C-1.29129e-06 7.60406 0.489671 8.09373 1.09375 8.09373L24.257 8.09374L20.42 11.9122C19.9919 12.3384 19.9903 13.0309 20.4164 13.459C20.8426 13.8873 21.5351 13.8887 21.9632 13.4627L27.6783 7.77524C27.6786 7.77491 27.6789 7.77453 27.6793 7.7742C28.1076 7.34666 28.1063 6.65191 27.6792 6.22578Z"/>\n' +
                    '                            </svg>\n' +
                    '                        </button>',

            })
        }
    })

    // табы Как работает сервис

    $(".how_it_works_link_item").click(function () {
        let thisIndex = $(this).index();
        $(".how_it_works_link_item").removeClass("active");
        $(this).addClass("active");

        $(".how_it_works_step_number").removeClass("active");
        let stepNum = $(".how_it_works_step_number")[thisIndex];
        $(stepNum).addClass("active");

        $(".how_it_works_tab_item").removeClass("active");
        let stepImg = $(".how_it_works_tab_item")[thisIndex];
        $(stepImg).addClass("active");
    });

    $(".how_it_works_step_number").click(function () {
        let thisIndex = $(this).index() / 2;
        $(".how_it_works_step_number").removeClass("active");
        $(this).addClass("active");

        $(".how_it_works_link_item").removeClass("active");
        let stepNum = $(".how_it_works_link_item")[thisIndex];
        $(stepNum).addClass("active");

        $(".how_it_works_tab_item").removeClass("active");
        let stepImg = $(".how_it_works_tab_item")[thisIndex];
        $(stepImg).addClass("active");
    });

    // FAQ

    $(".faq_accordion_item_head").click(function () {
        let thisParent = $(this).parents(".faq_accordion_item")[0];
        let thisContent = $(this).siblings(".faq_accordion_item_content")[0];

        if($(thisParent).hasClass("active")){
            $(thisParent).removeClass("active");
            $(thisContent).slideUp()
        } else {
            $(".faq_accordion_item").removeClass("active");
            $(".faq_accordion_item_content").slideUp()
            $(thisParent).addClass("active");
            $(thisContent).slideDown(300);
        }
    })

    // News slider

    $("#main_news_slider").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        dotsClass: 'examples_list_slider_dots',
        responsive: [
            {
                breakpoint: 1179,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1
                }
            }
        ],
        prevArrow: '<button class="examples_list_slider_btn prev_btn">\n' +
            '                            <svg width="28" height="14" viewBox="0 0 28 14">\n' +
            '                                <path d="M0.320796 7.77422L0.32178 7.77526L6.03684 13.4628C6.46499 13.8888 7.1575 13.8872 7.58368 13.459C8.0098 13.0309 8.00816 12.3384 7.58001 11.9123L3.74292 8.09376L26.9063 8.09376C27.5103 8.09376 28 7.60409 28 7.00001C28 6.39594 27.5103 5.90626 26.9063 5.90626L3.74298 5.90626L7.57996 2.08777C8.00811 1.66164 8.00975 0.969136 7.58363 0.540988C7.15745 0.112731 6.46488 0.111255 6.03679 0.53727L0.321725 6.22476L0.32074 6.2258C-0.107627 6.65335 -0.10626 7.3481 0.320796 7.77422Z"/>\n' +
            '                            </svg>\n' +
            '                        </button>',
        nextArrow: '<button class="examples_list_slider_btn next_btn">\n' +
            '                            <svg width="28" height="14" viewBox="0 0 28 14">\n' +
            '                                <path d="M27.6792 6.22578C27.6789 6.22545 27.6786 6.22507 27.6782 6.22474L21.9632 0.53725C21.535 0.11118 20.8425 0.112766 20.4163 0.540968C19.9902 0.969116 19.9918 1.66162 20.42 2.08775L24.2571 5.90624L1.09375 5.90624C0.489671 5.90624 -1.08005e-06 6.39591 -1.18567e-06 6.99999C-1.29129e-06 7.60406 0.489671 8.09373 1.09375 8.09373L24.257 8.09374L20.42 11.9122C19.9919 12.3384 19.9903 13.0309 20.4164 13.459C20.8426 13.8873 21.5351 13.8887 21.9632 13.4627L27.6783 7.77524C27.6786 7.77491 27.6789 7.77453 27.6793 7.7742C28.1076 7.34666 28.1063 6.65191 27.6792 6.22578Z"/>\n' +
            '                            </svg>\n' +
            '                        </button>',
    })

    //We help slider

    $("#indiv_we_help_slider").slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: true,
        dotsClass: 'examples_list_slider_dots',
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2
                }
            },
        ],
        prevArrow: '<button class="examples_list_slider_btn prev_btn">\n' +
            '                            <svg width="28" height="14" viewBox="0 0 28 14">\n' +
            '                                <path d="M0.320796 7.77422L0.32178 7.77526L6.03684 13.4628C6.46499 13.8888 7.1575 13.8872 7.58368 13.459C8.0098 13.0309 8.00816 12.3384 7.58001 11.9123L3.74292 8.09376L26.9063 8.09376C27.5103 8.09376 28 7.60409 28 7.00001C28 6.39594 27.5103 5.90626 26.9063 5.90626L3.74298 5.90626L7.57996 2.08777C8.00811 1.66164 8.00975 0.969136 7.58363 0.540988C7.15745 0.112731 6.46488 0.111255 6.03679 0.53727L0.321725 6.22476L0.32074 6.2258C-0.107627 6.65335 -0.10626 7.3481 0.320796 7.77422Z"/>\n' +
            '                            </svg>\n' +
            '                        </button>',
        nextArrow: '<button class="examples_list_slider_btn next_btn">\n' +
            '                            <svg width="28" height="14" viewBox="0 0 28 14">\n' +
            '                                <path d="M27.6792 6.22578C27.6789 6.22545 27.6786 6.22507 27.6782 6.22474L21.9632 0.53725C21.535 0.11118 20.8425 0.112766 20.4163 0.540968C19.9902 0.969116 19.9918 1.66162 20.42 2.08775L24.2571 5.90624L1.09375 5.90624C0.489671 5.90624 -1.08005e-06 6.39591 -1.18567e-06 6.99999C-1.29129e-06 7.60406 0.489671 8.09373 1.09375 8.09373L24.257 8.09374L20.42 11.9122C19.9919 12.3384 19.9903 13.0309 20.4164 13.459C20.8426 13.8873 21.5351 13.8887 21.9632 13.4627L27.6783 7.77524C27.6786 7.77491 27.6789 7.77453 27.6793 7.7742C28.1076 7.34666 28.1063 6.65191 27.6792 6.22578Z"/>\n' +
            '                            </svg>\n' +
            '                        </button>',
    })

    // mobile nav

    $(".mobile_header_ham").click(function () {
        $(this).toggleClass("active");
        $(".mobile_header").toggleClass("active");
        $(".mobile_header_bottom_slide").slideToggle()
    })

    $(".mobile_header_nav a").each(function (){
        let thisSibling = $(this).siblings("div");
        let thisParent = $(this).parent("li")

        if (!!thisSibling.length){
            thisParent.addClass('sublist')
        }
    })

    $(".mobile_header_nav li.sublist > a").click(function (e){
        e.preventDefault();
        $(this).parent('.sublist').toggleClass("active")
        $(this).siblings('div').slideToggle()
    })

    // calc additional

    $('#calculate_price_form_additional_origin_slider').slider({
        value: 50,
        min: 0,
        max: 90,
        slide: function (event, ui) {
            $(".calculate_price_form_additional_origin_text span").text(ui.value)
            $(".calculate_price_form_additional_origin_slider").css({"backgroundImage": `linear-gradient(to right, #052852 0%, #052852 ${ui.handle.style.left}, #fff ${ui.handle.style.left}, #fff 100%)`
        })
        }
    });

    $(".calculate_price_form_additional_text").click(function (){
        $(this).toggleClass("active");
        $(".calculate_price_form_additional_wrap").slideToggle()
    })
});