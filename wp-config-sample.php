<?php
/** 
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information by
 * visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'students');

/** MySQL database username */
define('DB_USER', 'user');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link http://api.wordpress.org/secret-key/1.1/ WordPress.org secret-key service}
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'nsGrFLx6QCn7pw0*dnUqDbt$c!4G&sLuVyNGajk!qU^Six2jqPuOGHZ^g7IbZJ39');
define('SECURE_AUTH_KEY',  'RV5E9wo#DWruGHTKRlt4RYXlDeUr4ROfEaDye40un1KSC8!BeHShAJDVL6D%Hhl4');
define('LOGGED_IN_KEY',    'coGFnFZHG)k0XdGSzBUhLh7w^A#8GPyll3BiAJLX0JSOFL&GMwDcU(HBCZ0@8qZp');
define('NONCE_KEY',        'oYH!Z9iTtNSR*WzqTXbb!^zx!2j7Jhj5KA$o@$L$ldxnz8Fddty0mpRLCUk$otGG');
define('AUTH_SALT',        'r0)!oOWENEgR8nS(9Pwd%UVPhC6@v*lgV7fsaC)tr&7mcgcWeKr6eAzK^RcuRKow');
define('SECURE_AUTH_SALT', 'o4*AX!Axd8!dwu&bwnf$a@2*MO4WXqkw7oU370MOCCpBVMW6)btTG%ghmkIDxluW');
define('LOGGED_IN_SALT',   'iJh0)G(EPQ(*LGxG9Sws1PUvXeyxf&%vQuW2X(&JTK37RDIPX4ppk#ROP@f5E!LX');
define('NONCE_SALT',       '0h02eMK1BBejPqjrYJ0j4!!$vP54cT)*Wwj1ABDqQ$KqS#YBmsD&2eul3ntW@2uV');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', 'ru_RU');

define( 'WP_ALLOW_MULTISITE', true );

define ('FS_METHOD', 'direct');

define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

//--- disable auto upgrade
define( 'AUTOMATIC_UPDATER_DISABLED', true );



?>
